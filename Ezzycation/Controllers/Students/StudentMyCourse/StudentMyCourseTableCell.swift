//
//  StudentMyCourseTableCell.swift
//  Ezzycation
//
//  Created by apple on 30/03/21.
//

import UIKit

class StudentMyCourseTableCell: UITableViewCell {

    @IBOutlet weak var lblSubjectName:UILabel! {
        didSet {
            lblSubjectName.text = "Subject Name -"
        }
    }
    
    @IBOutlet weak var lblSubjectPrice:UILabel! {
        didSet {
            lblSubjectPrice.text = "$199"
            lblSubjectPrice.textColor = NAVIGATION_COLOR
        }
    }
    
    @IBOutlet weak var lblDuration:UILabel!
    
    @IBOutlet weak var btnCell:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
