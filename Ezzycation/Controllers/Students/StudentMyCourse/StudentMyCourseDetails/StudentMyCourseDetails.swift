//
//  StudentMyCourseDetails.swift
//  Ezzycation
//
//  Created by Apple on 27/07/21.
//

import UIKit
import Alamofire
import SDWebImage

class StudentMyCourseDetails: UIViewController {

    var getMyCourseDetails:NSDictionary!
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "DETAILS"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.backgroundColor = .black
        }
    }
    
    @IBOutlet weak var lblSubject:UILabel!
    
    @IBOutlet weak var lblCourseDuration:UILabel! {
        didSet {
            lblCourseDuration.text = "6 Months"
            lblCourseDuration.textColor = NAVIGATION_COLOR
        }
    }
    @IBOutlet weak var lblStudent:UILabel! {
        didSet {
            lblStudent.backgroundColor = NAVIGATION_COLOR
            lblStudent.textAlignment = .center
            lblStudent.text = "0"
            lblStudent.layer.cornerRadius = 20
            lblStudent.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var viewOrangeBG:UIView! {
        didSet {
            viewOrangeBG.backgroundColor = .systemOrange
        }
    }
    
    @IBOutlet weak var imgTeacherImage:UIImageView! {
        didSet {
            imgTeacherImage.layer.cornerRadius = 35
            imgTeacherImage.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblTeacherName:UILabel! {
        didSet {
            lblTeacherName.textColor = .black
        }
    }
    
    @IBOutlet weak var lblTeacherExperience:UILabel! {
        didSet {
            lblTeacherExperience.textColor = .black
        }
    }
    
    @IBOutlet weak var viewBGTime:UIView! {
        didSet {
            viewBGTime.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var btnTimeOne:UIButton! {
        didSet {
            btnTimeOne.isUserInteractionEnabled = false
            btnTimeOne.layer.cornerRadius = 6
            btnTimeOne.clipsToBounds = true
            btnTimeOne.backgroundColor = .black
            btnTimeOne.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var btnTimeTwo:UIButton! {
        didSet {
            btnTimeTwo.isUserInteractionEnabled = false
            btnTimeTwo.layer.cornerRadius = 6
            btnTimeTwo.clipsToBounds = true
            btnTimeTwo.backgroundColor = .black
            btnTimeTwo.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var lblDays:UILabel! {
        didSet {
            lblDays.textColor = .black
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = .white
        
                
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        if let myLoadedString = UserDefaults.standard.string(forKey: "keyBackOrSlide") {
            if myLoadedString == "backOrMenu" {
                
                // menu
                btnBack.setImage(UIImage(named: "menu"), for: .normal)
                self.sideBarMenuClick()
            } else {
                
                // back
                btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
                btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
            }
            
         } else {
            
            // back
            btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
        
        
        
        // print(getMyCourseDetails as Any)
        
        self.lblSubject.text = (getMyCourseDetails!["subjaectName"] as! String)+" - "+(getMyCourseDetails!["ClassType"] as! String)
        
        self.lblCourseDuration.text = (getMyCourseDetails!["duration"] as! String)
        self.lblStudent.text = "\(getMyCourseDetails!["totalNoOfStudent"] as! Int)"
        
        self.imgTeacherImage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        self.imgTeacherImage.sd_setImage(with: URL(string: (getMyCourseDetails["TeacherImage"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        
        // days
        var mondayText:String! = ""
        var tuesdayText:String! = ""
        var wednesdayText:String! = ""
        var thursdayText:String! = ""
        var fridayText:String! = ""
        var saturdayText:String! = ""
        var sundayText:String! = ""
        
        
        if "\(getMyCourseDetails!["Mon"] as! Int)" == "0" {
            mondayText = "0, "
        } else {
            mondayText = "Monday, "
        }
        
        if "\(getMyCourseDetails!["Tue"] as! Int)" == "0" {
            tuesdayText = "0, "
        } else {
            tuesdayText = "Tuesday, "
        }
        
        if "\(getMyCourseDetails!["Wed"] as! Int)" == "0" {
            wednesdayText = "0, "
        } else {
            wednesdayText = "Wednesday, "
        }
        
        if "\(getMyCourseDetails!["Thu"] as! Int)" == "0" {
            thursdayText = "0, "
        } else {
            thursdayText = "Thursday, "
        }
        
        if "\(getMyCourseDetails!["Fri"] as! Int)" == "0" {
            fridayText = "0, "
        } else {
            fridayText = "Friday, "
        }
        
        if "\(getMyCourseDetails!["Sat"] as! Int)" == "0" {
            saturdayText = "0, "
        } else {
            saturdayText = "Saturday, "
        }
        
        if "\(getMyCourseDetails!["Sun"] as! Int)" == "0" {
            sundayText = "0, "
        } else {
            sundayText = "Saturday, "
        }
        
        
            
        // days
        let weekdays:String = String(mondayText)+String(tuesdayText)+String(wednesdayText)+String(thursdayText)+String(fridayText)+String(saturdayText)+String(sundayText)
        
        let str2 = String(weekdays).replacingOccurrences(of: "0, ", with: "", options: .literal, range: nil)
        
        self.lblDays.text = String(str2)
        
        
        // time
        self.btnTimeOne.setTitle((getMyCourseDetails!["StartTime"] as! String), for: .normal)
        self.btnTimeTwo.setTitle((getMyCourseDetails!["EndTime"] as! String), for: .normal)
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc func sideBarMenuClick() {
        
        let defaults = UserDefaults.standard
        defaults.setValue("", forKey: "keyBackOrSlide")
        defaults.setValue(nil, forKey: "keyBackOrSlide")
        
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
}
