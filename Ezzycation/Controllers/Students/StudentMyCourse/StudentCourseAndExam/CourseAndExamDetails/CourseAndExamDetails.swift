//
//  CourseAndExamDetails.swift
//  Ezzycation
//
//  Created by apple on 27/07/21.
//

import UIKit
import Alamofire
import SDWebImage

class CourseAndExamDetails: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "MY COURSE/EXAMS"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var arrListOfAllMySubjects:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var strMainSubjectId:String!
    var strCourseDurationId:String!
    
    var strDaySun:String! = "0"
    var strDayMon:String! = "0"
    var strDayTue:String! = "0"
    var strDayWed:String! = "0"
    var strDayThu:String! = "0"
    var strDayFri:String! = "0"
    var strDaySat:String! = "0"
    
    var strStartTime:String! = ""
    var strEndTime:String! = ""
    
    var strSubject:String!
    var lblSubjectCourseDuration:String!
    var totalNumberOfStudents:String!
    
    var strTotalNumberOfStudents:String! = "0"
    
    var strClassType:String!
    
    var strEnrollId:String!
    
    var strIndexNumber:Int!
    var strChangeColorForTitle:Int!
    
    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.backgroundColor = .black
        }
    }
    
    @IBOutlet weak var lblSubject:UILabel!
    
    @IBOutlet weak var lblCourseDuration:UILabel! {
        didSet {
            lblCourseDuration.text = "6 Months"
            lblCourseDuration.textColor = NAVIGATION_COLOR
        }
    }
    @IBOutlet weak var lblStudent:UILabel! {
        didSet {
            lblStudent.backgroundColor = NAVIGATION_COLOR
            lblStudent.textAlignment = .center
            lblStudent.text = "0"
            lblStudent.layer.cornerRadius = 20
            lblStudent.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect.zero)
             // self.tbleView.delegate = self
             // self.tbleView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.strIndexNumber = 1200
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        if let myLoadedString = UserDefaults.standard.string(forKey: "keyBackOrSlide") {
            if myLoadedString == "backOrMenu" {
                
                // menu
                btnBack.setImage(UIImage(named: "menu"), for: .normal)
                self.sideBarMenuClick()
            } else {
                
                // back
                btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
                btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
            }
            
         } else {
            
            // back
            btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
        
        print(self.strMainSubjectId as Any)
        self.reloadAllData()
        // self.getAllSubjectListWB()
    }
    
    @objc func reloadAllData() {
        
        self.lblSubject.text = String(self.strSubject)
        self.lblCourseDuration.text = String(self.lblSubjectCourseDuration)
        self.lblStudent.text = String("")
        
        self.getAllSubjectListWB()
    }
    
    @objc func sideBarMenuClick() {
        
        self.view.endEditing(true)
        if revealViewController() != nil {
            
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    @objc func getAllSubjectListWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
            
        self.view.endEditing(true)
            
         // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             // let x : Int = (person["userId"] as! Int)
             // let myString = String(x)
                
            let params = StudentAllTeacherListWB(action: "findteacher",
                                                 courseDurationId: String(self.strCourseDurationId))
            
        print(params as Any)
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllMySubjects.addObjects(from: ar as! [Any])
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                    
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            // Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
         // }
            
    }
}

//MARK:- TABLE VIEW -
extension CourseAndExamDetails: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListOfAllMySubjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CourseAndExamDetailsTableCell = tableView.dequeueReusableCell(withIdentifier: "courseAndExamDetailsTableCell") as! CourseAndExamDetailsTableCell
        
        cell.backgroundColor = .clear
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        // cell.accessoryType = .disclosureIndicator
        
        let item = self.arrListOfAllMySubjects[indexPath.row] as? [String:Any]
        // print(item as Any)
        
        // teacher name
        cell.lblTeacherName.text = (item!["userName"] as! String)
        
        // teacher experience
        if (item!["userExperience"] as! String) == "0" {
            cell.lblTeacherExperience.text = "Total Experience :"+"\(item!["userExperience"] as! String)"+" Year"
        } else if "\(item!["userExperience"] as! String)" == "1" {
            cell.lblTeacherExperience.text = "Total Experience :"+"\(item!["userExperience"] as! String)"+" Year"
        } else {
            cell.lblTeacherExperience.text = "Total Experience :"+"\(item!["userExperience"] as! String)"+" Years"
        }
        
        // teacher profile image
        cell.imgTeacherProfileImage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        cell.imgTeacherProfileImage.sd_setImage(with: URL(string: (item!["profile_picture"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        // click cell
        cell.btnClick.tag = indexPath.row
        cell.btnClick.addTarget(self, action: #selector(clickCellMethod), for: .touchUpInside)
        
        // check days
        if "\(item!["Mon"] as! String)" == "0" {
            cell.btnMonCheck.setImage(UIImage(systemName: "pencil.slash"), for: .normal)
            cell.btnMonCheck.tintColor = .systemRed
        } else {
            cell.btnMonCheck.setImage(UIImage(systemName: "checkmark"), for: .normal)
            cell.btnMonCheck.tintColor = .systemGreen
        }
        
        if "\(item!["Tue"] as! String)" == "0" {
            cell.btnTueCheck.setImage(UIImage(systemName: "pencil.slash"), for: .normal)
            cell.btnMonCheck.tintColor = .systemRed
        } else {
            cell.btnTueCheck.setImage(UIImage(systemName: "checkmark"), for: .normal)
            cell.btnMonCheck.tintColor = .systemGreen
        }
        
        if "\(item!["Wed"] as! String)" == "0" {
            cell.btnWedCheck.setImage(UIImage(systemName: "pencil.slash"), for: .normal)
            cell.btnMonCheck.tintColor = .systemRed
        } else {
            cell.btnWedCheck.setImage(UIImage(systemName: "checkmark"), for: .normal)
            cell.btnMonCheck.tintColor = .systemGreen
        }
        
        if "\(item!["Thu"] as! String)" == "0" {
            cell.btnThuCheck.setImage(UIImage(systemName: "pencil.slash"), for: .normal)
            cell.btnMonCheck.tintColor = .systemRed
        } else {
            cell.btnThuCheck.setImage(UIImage(systemName: "checkmark"), for: .normal)
            cell.btnMonCheck.tintColor = .systemGreen
        }
        
        if "\(item!["Fri"] as! String)" == "0" {
            cell.btnFriCheck.setImage(UIImage(systemName: "pencil.slash"), for: .normal)
            cell.btnMonCheck.tintColor = .systemRed
        } else {
            cell.btnFriCheck.setImage(UIImage(systemName: "checkmark"), for: .normal)
            cell.btnMonCheck.tintColor = .systemGreen
        }
        
        if "\(item!["Sat"] as! String)" == "0" {
            cell.btnSatCheck.setImage(UIImage(systemName: "pencil.slash"), for: .normal)
            cell.btnMonCheck.tintColor = .systemRed
        } else {
            cell.btnSatCheck.setImage(UIImage(systemName: "checkmark"), for: .normal)
            cell.btnMonCheck.tintColor = .systemGreen
        }
        
        if "\(item!["Sun"] as! String)" == "0" {
            cell.btnSunCheck.setImage(UIImage(systemName: "pencil.slash"), for: .normal)
            cell.btnMonCheck.tintColor = .systemRed
        } else {
            cell.btnSunCheck.setImage(UIImage(systemName: "checkmark"), for: .normal)
            cell.btnMonCheck.tintColor = .systemGreen
        }
        
        
        
        
        return cell
    }
    
    @objc func clickCellMethod(_ sender:UIButton) {
        
        //print(sender.tag as Any)
        
        if self.strIndexNumber == sender.tag {
            
            print("yes same")
            self.strIndexNumber = 1200
            
        } else {
            
            print("i am different")
            self.strIndexNumber = sender.tag
            
            self.strChangeColorForTitle = sender.tag
            
        }
        
        self.tbleView.reloadData()
        
        /*let item2 = self.arrListOfAllMySubjects[sender.tag] as? [String:Any]
         // print(item2 as Any)
        
        var ar : NSArray!
        ar = (item2!["Coursedurations"] as! Array<Any>) as NSArray
        
        // print(ar as Any)
        // self.arrListOfAllMySubjects.addObjects(from: ar as! [Any])
        
        let alert = UIAlertController(title: "Please select Duration", message: nil, preferredStyle: .actionSheet)
        
        for indexx in 0..<ar.count {
            
            let item = ar[indexx] as? [String:Any]
            
            
            let x : Int = (item!["price"] as! Int)
            let myString = String(x)
            
            let courseStatement = (item!["duration"] as! String)+" - "+(item!["ClassType"] as! String)+" - $"+String(myString)
            
            alert.addAction(UIAlertAction(title: String(courseStatement), style: .default , handler:{ (UIAlertAction)in
                print("User click Approve button")
                
                // var strMainSubjectId:String!
                // var strCourseDurationId:String!
                
                 // print(item as Any)
                
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubjectDetailsId") as? SubjectDetails
                
                let getSubjectId : Int = (item2!["id"] as! Int)
                let myString = String(getSubjectId)
                
                let getSelectedCourseDurationId : Int = (item!["id"] as! Int)
                let getCID = String(getSelectedCourseDurationId)
                
                push!.strMainSubjectId = String(myString)
                push!.strCourseDurationId = String(getCID)
                
                push!.lblSubject = (item2!["name"] as! String)+" - "+(item!["ClassType"] as! String) // subject name
                push!.lblSubjectCourseDuration = (item!["duration"] as! String) // course duration
                push!.strClassType = (item!["ClassType"] as! String)
                 
                
                
                self.navigationController?.pushViewController(push!, animated: true)
                
            }))
            
        }
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })*/
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        print("yes called")
        if self.strIndexNumber == indexPath.row {
            print("yes same")
            self.strIndexNumber = 1200
        } else {
            print("i am different")
            self.strIndexNumber = indexPath.row
            
            self.strChangeColorForTitle = indexPath.row
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == self.strIndexNumber {
            return 350
        } else {
            return 120
        }
        
    }
    
}

extension CourseAndExamDetails: UITableViewDelegate {
    
}
