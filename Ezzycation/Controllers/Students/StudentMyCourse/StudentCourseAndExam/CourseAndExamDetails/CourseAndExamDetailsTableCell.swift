//
//  CourseAndExamDetailsTableCell.swift
//  Ezzycation
//
//  Created by apple on 27/07/21.
//

import UIKit

class CourseAndExamDetailsTableCell: UITableViewCell {

    @IBOutlet weak var imgTeacherProfileImage:UIImageView! {
        didSet {
            imgTeacherProfileImage.layer.borderWidth = 1
            imgTeacherProfileImage.layer.borderColor = UIColor.lightGray.cgColor
            imgTeacherProfileImage.layer.cornerRadius = 35
            imgTeacherProfileImage.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblTeacherName:UILabel!
    @IBOutlet weak var lblTeacherExperience:UILabel!
    
    @IBOutlet weak var btnStarOne:UIButton! {
        didSet {
            btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
            btnStarOne.tintColor = .systemYellow
        }
    }
    
    @IBOutlet weak var btnStarTwo:UIButton! {
        didSet {
            btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            btnStarTwo.tintColor = .systemYellow
        }
    }
    
    @IBOutlet weak var btnStarThree:UIButton! {
        didSet {
            btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            btnStarThree.tintColor = .systemYellow
        }
    }
    
    @IBOutlet weak var btnStarFour:UIButton! {
        didSet {
            btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            btnStarFour.tintColor = .systemYellow
        }
    }
    
    @IBOutlet weak var btnStarFive:UIButton! {
        didSet {
            btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            btnStarFive.tintColor = .systemYellow
        }
    }
    
    @IBOutlet weak var lblSunday:UILabel! {
        didSet {
            lblSunday.textColor = .black
            lblSunday.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var lblMonday:UILabel! {
        didSet {
            lblMonday.textColor = .black
            lblMonday.backgroundColor = .clear
        }
    }
    @IBOutlet weak var lblTuesday:UILabel! {
        didSet {
            lblTuesday.textColor = .black
            lblTuesday.backgroundColor = .clear
        }
    }
    @IBOutlet weak var lblWednesday:UILabel! {
        didSet {
            lblWednesday.textColor = .black
            lblWednesday.backgroundColor = .clear
        }
    }
    @IBOutlet weak var lblThursday:UILabel! {
        didSet {
            lblThursday.textColor = .black
            lblThursday.backgroundColor = .clear
        }
    }
    @IBOutlet weak var lblFriday:UILabel! {
        didSet {
            lblFriday.textColor = .black
            lblFriday.backgroundColor = .clear
        }
    }
    @IBOutlet weak var lblSaturday:UILabel! {
        didSet {
            lblSaturday.textColor = .black
            lblSaturday.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var btnClick:UIButton!
    
    @IBOutlet weak var btnMonCheck:UIButton!
    @IBOutlet weak var btnTueCheck:UIButton!
    @IBOutlet weak var btnWedCheck:UIButton!
    @IBOutlet weak var btnThuCheck:UIButton!
    @IBOutlet weak var btnFriCheck:UIButton!
    @IBOutlet weak var btnSatCheck:UIButton!
    @IBOutlet weak var btnSunCheck:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
