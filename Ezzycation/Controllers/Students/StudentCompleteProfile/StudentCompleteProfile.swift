//
//  StudentCompleteProfile.swift
//  Ezzycation
//
//  Created by apple on 30/03/21.
//

import UIKit

class StudentCompleteProfile: UIViewController {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "COMPLETE PROFILE"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    @IBOutlet weak var txtGender:UITextField! {
        didSet {
            txtGender.layer.cornerRadius = 6
            txtGender.clipsToBounds = true
            txtGender.backgroundColor = .white
            txtGender.setLeftPaddingPoints(20)
            txtGender.attributedPlaceholder = NSAttributedString(string: "Gender",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    
    @IBOutlet weak var txtDateOfBirth:UITextField! {
        didSet {
            txtDateOfBirth.layer.cornerRadius = 6
            txtDateOfBirth.clipsToBounds = true
            txtDateOfBirth.backgroundColor = .white
            txtDateOfBirth.setLeftPaddingPoints(20)
            txtDateOfBirth.attributedPlaceholder = NSAttributedString(string: "Date of birth",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var btnSaveAndContinue:UIButton!  {
        didSet {
            btnSaveAndContinue.backgroundColor = NAVIGATION_COLOR
            btnSaveAndContinue.setTitle("FINISH", for: .normal)
            btnSaveAndContinue.layer.cornerRadius = 6
            btnSaveAndContinue.clipsToBounds = true
            btnSaveAndContinue.setTitleColor(.white, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.btnBack.addTarget(self, action: #selector(btnBackClickMethod), for: .touchUpInside)
        
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func btnBackClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
}
