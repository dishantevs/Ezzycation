//
//  TeacherEnrollementList.swift
//  Ezzycation
//
//  Created by apple on 22/07/21.
//

import UIKit
import Alamofire

class TeacherEnrollementList: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "COURSE/EXAM TIMING"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var arrListOfAllEnrollment:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect.zero)
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        if let myLoadedString = UserDefaults.standard.string(forKey: "keyBackOrSlide") {
            if myLoadedString == "backOrMenu" {
                
                // menu
                btnBack.setImage(UIImage(named: "menu"), for: .normal)
                self.sideBarMenuClick()
            } else {
                
                // back
                btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
                btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
            }
            
         } else {
            
            // back
            btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
        
        self.getTeacherEnrollmentListWB()
    }
    
    @objc func sideBarMenuClick() {
        
        self.view.endEditing(true)
        if revealViewController() != nil {
            
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    @objc func getTeacherEnrollmentListWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
            
        self.view.endEditing(true)
            
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            let x : Int = (person["userId"] as! Int)
            let myString = String(x)
                
            let params = TeacherEnrollmentListWB(action: "teacherenrolllist",
                                                 userId: String(myString))
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                ERProgressHud.sharedInstance.hide()
                               
                                
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllEnrollment.addObjects(from: ar as! [Any])
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                // Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            // Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
         }
            
    }
}


//MARK:- TABLE VIEW -
extension TeacherEnrollementList: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListOfAllEnrollment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TeacherEnrollementListtableCell = tableView.dequeueReusableCell(withIdentifier: "teacherEnrollementListtableCell") as! TeacherEnrollementListtableCell
        
        cell.backgroundColor = .clear
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.accessoryType = .disclosureIndicator
        
        let item = self.arrListOfAllEnrollment[indexPath.row] as? [String:Any]
        print(item as Any)
        
        // for course duration nad price
        var dictCD: Dictionary<AnyHashable, Any>
        dictCD = item!["courseduration"] as! Dictionary<AnyHashable, Any>
        
        // subject group or private
        var dictSUB: Dictionary<AnyHashable, Any>
        dictSUB = item!["subject"] as! Dictionary<AnyHashable, Any>
        
        cell.lblSubjectName.text = (dictSUB["name"] as! String)+" - "+(dictCD["ClassType"] as! String)
        cell.lblMonths.text = (dictCD["duration"] as! String)+" - $"+"\(dictCD["price"] as! Int)"
        
        cell.btnClickCell.tag = indexPath.row
        cell.btnClickCell.addTarget(self, action: #selector(clickCellMethod), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickCellMethod(_ sender:UIButton) {
        
        let item = self.arrListOfAllEnrollment[sender.tag] as? [String:Any]
        print(item as Any)
        
        // for course duration nad price
        var dictCD: Dictionary<AnyHashable, Any>
        dictCD = item!["courseduration"] as! Dictionary<AnyHashable, Any>
        
        // subject group or private
        var dictSUB: Dictionary<AnyHashable, Any>
        dictSUB = item!["subject"] as! Dictionary<AnyHashable, Any>
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubjectDetailsId") as? SubjectDetails
        
        push!.strMainSubjectId = "\(dictSUB["id"] as! Int)"
        push!.strCourseDurationId = "\(dictCD["id"] as! Int)"
        push!.lblSubject = "\(dictSUB["name"] as! String)"
        push!.lblSubjectCourseDuration = "\(dictCD["duration"] as! String)"+" - "+"\(dictCD["ClassType"] as! String)"
        push!.strClassType = "\(item!["ClassType"] as! String)"
        
        self.navigationController?.pushViewController(push!, animated: true)
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        print("yes called")
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60+20
    }
    
}

extension TeacherEnrollementList: UITableViewDelegate {
    
}
