//
//  TeacherEnrolledStudents.swift
//  Ezzycation
//
//  Created by apple on 30/03/21.
//

import UIKit

class TeacherEnrolledStudents: UIViewController {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "ENROLLED STUDENTS"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            self.tbleView.backgroundColor = .white
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.btnBack.addTarget(self, action: #selector(btnBackClickMethod), for: .touchUpInside)
        
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func btnBackClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
}

//MARK:- TABLE VIEW -
extension TeacherEnrolledStudents: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TeacherEnrolledStudentsTableCell = tableView.dequeueReusableCell(withIdentifier: "teacherEnrolledStudentsTableCell") as! TeacherEnrolledStudentsTableCell
        
        cell.backgroundColor = .clear
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        print("yes called")
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

extension TeacherEnrolledStudents: UITableViewDelegate {
    
}
