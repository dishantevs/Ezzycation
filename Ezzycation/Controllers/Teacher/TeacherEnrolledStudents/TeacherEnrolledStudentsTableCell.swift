//
//  TeacherEnrolledStudentsTableCell.swift
//  Ezzycation
//
//  Created by apple on 30/03/21.
//

import UIKit

class TeacherEnrolledStudentsTableCell: UITableViewCell {

    @IBOutlet weak var imgView:UIImageView! {
        didSet {
            imgView.layer.cornerRadius = 30
            imgView.clipsToBounds = true
            imgView.layer.borderWidth = 0.8
            imgView.layer.borderColor = UIColor.lightGray.cgColor
            imgView.backgroundColor = .lightGray
        }
    }
    
    @IBOutlet weak var lblStudentName:UILabel!
    @IBOutlet weak var lblTeacherphoneNumber:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
