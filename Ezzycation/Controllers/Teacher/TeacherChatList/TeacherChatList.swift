//
//  TeacherChatList.swift
//  Ezzycation
//
//  Created by apple on 30/03/21.
//

import UIKit

class TeacherChatList: UIViewController {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "CHAT"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            self.tbleView.backgroundColor = .white
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
        }
    }
    
    @IBOutlet weak var btnPrivateChat:UIButton! {
        didSet {
            btnPrivateChat.layer.cornerRadius = 6
            btnPrivateChat.clipsToBounds = true
            btnPrivateChat.setTitle("Private Chat", for: .normal)
            btnPrivateChat.setTitleColor(.white, for: .normal)
            btnPrivateChat.backgroundColor = .black
            
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: btnPrivateChat.frame.height - 1, width: btnPrivateChat.frame.width, height: 4.0)
            bottomLine.backgroundColor = NAVIGATION_COLOR.cgColor
            btnPrivateChat.layer.addSublayer(bottomLine)
        }
    }
    
    @IBOutlet weak var btnGroupChat:UIButton! {
        didSet {
            btnGroupChat.layer.cornerRadius = 6
            btnGroupChat.clipsToBounds = true
            btnGroupChat.setTitle("Group Chat", for: .normal)
            btnGroupChat.setTitleColor(.white, for: .normal)
            btnGroupChat.backgroundColor = .black
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.btnBack.addTarget(self, action: #selector(btnBackClickMethod), for: .touchUpInside)
                
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func btnBackClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
}

//MARK:- TABLE VIEW -
extension TeacherChatList: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TeacherChatListTableCell = tableView.dequeueReusableCell(withIdentifier: "teacherChatListTableCell") as! TeacherChatListTableCell
        
        cell.backgroundColor = .clear
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        print("yes called")
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

extension TeacherChatList: UITableViewDelegate {
    
}
