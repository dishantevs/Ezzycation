//
//  TeacherSelectSubject.swift
//  Ezzycation
//
//  Created by apple on 01/04/21.
//

import UIKit
import Alamofire

class TeacherSelectSubject: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "SELECT SUBJECT"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var arrListOfSubjects:NSMutableArray = []
    
    let weekdaysArray = ["Hindi", "English", "Maths", "SST", "Science", "Physics", "Chemistry", "EVS"]
    
    @IBOutlet weak var btnSaveAndContinue:UIButton! {
        didSet {
            btnSaveAndContinue.backgroundColor = NAVIGATION_COLOR
            btnSaveAndContinue.setTitle("Save & Continue", for: .normal)
            btnSaveAndContinue.layer.cornerRadius = 6
            btnSaveAndContinue.clipsToBounds = true
            btnSaveAndContinue.setTitleColor(.white, for: .normal)
        }
    }
    
    // MARK:- COLLECTION VIEW SETUP -
    @IBOutlet weak var clView: UICollectionView! {
        didSet {
               //collection
            // clView!.dataSource = self
            // clView!.delegate = self
            clView!.backgroundColor = .clear
            clView.isPagingEnabled = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.btnBack.addTarget(self, action: #selector(btnBackClickMethod), for: .touchUpInside)
        self.btnSaveAndContinue.addTarget(self, action: #selector(saveAndContinueClickMethod), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        self.classListWB()
    }
    
    @objc func saveAndContinueClickMethod() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompleteProfileId")
        self.navigationController?.pushViewController(push, animated: true)
        
    }
    
    @objc func btnBackClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    
    
    @objc func classListWB() {
        // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let params = SubjectListWB(action: "category")
        
        print(params as Any)
        
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                            
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            // self.arrListOfSubjects.addObjects(from: ar as! [Any])
                            
                            let statusText = "no"
                            
                            for indexx in 0..<ar.count {
                                
                                let item = ar[indexx] as? [String:Any]
                                
                                let x2 : Int = (item!["id"] as! Int)
                                let myString2 = String(x2)
                                
                                let myDictionary: [String:String] = [
                                    
                                    "id"        : String(indexx+1),
                                    "name"      : (item!["name"] as! String),
                                    "classId"   : String(myString2),
                                    "status"    : String(statusText)
                                    
                                ]
                                
                                var res = [[String: String]]()
                                res.append(myDictionary)
                                
                                self.arrListOfSubjects.addObjects(from: res)
                                
                            }
                            
                            self.clView.delegate = self
                            self.clView.dataSource = self
                            self.clView.reloadData()
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        
                    }
        }
        
    }
    
}

extension TeacherSelectSubject: UICollectionViewDelegate {
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
         return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrListOfSubjects.count
    }
    
    //Write Delegate Code Here
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "teacherSelectSubjectCollectionCell", for: indexPath as IndexPath) as! TeacherSelectSubjectCollectionCell
       
        // MARK:- CELL CLASS -
        cell.layer.cornerRadius = 30
        cell.clipsToBounds = true
        cell.backgroundColor = .clear
        cell.layer.borderColor = UIColor.white.cgColor
        cell.layer.borderWidth = 0.6
        
        let item = self.arrListOfSubjects[indexPath.row] as? [String:Any]
        
        cell.lblClass.textColor = .white
        cell.lblClass.textAlignment = .center
        
        cell.lblClass.text = (item!["name"] as! String)
        cell.btnClass.setTitle("", for: .normal)
        
        cell.btnClass.setTitleColor(.white, for: .normal)
        
        if item!["status"] as! String == "yes" {
            
            cell.layer.borderColor = UIColor.white.cgColor
            cell.layer.borderWidth = 0.6
            cell.backgroundColor = NAVIGATION_COLOR
            
        } else {
            
            cell.layer.borderColor = NAVIGATION_COLOR.cgColor
            cell.layer.borderWidth = 0.6
            cell.backgroundColor = .clear
            
        }
        
        cell.btnClass.tag = indexPath.row
        cell.btnClass.addTarget(self, action: #selector(btnTitleClickMethod), for: .touchUpInside)
        
        return cell
    }
    
    @objc func btnTitleClickMethod(_ sender:UIButton) {
        let btn:UIButton = sender
        
        /*let index = collectionView.tag
        print(index as Any)*/
        
        let item = self.arrListOfSubjects[btn.tag] as? [String:Any]
        print(item as Any)
        
        if item!["status"] as! String == "no" {
            
            self.arrListOfSubjects.removeObject(at: btn.tag)
            
            let myDictionary: [String:String] = [
                
                "id"        : item!["id"] as! String,
                "name"      : item!["name"] as! String,
                "status"    : "yes"
                
            ]
           
            self.arrListOfSubjects.insert(myDictionary, at: btn.tag)
            
        } else {
        
            self.arrListOfSubjects.removeObject(at: btn.tag)
            
            let myDictionary: [String:String] = [
                
                "id"        : item!["id"] as! String,
                "name"      : item!["name"] as! String,
                "status"    : "no"
                
            ]
           
            self.arrListOfSubjects.insert(myDictionary, at: btn.tag)
            
        }
        
        // print(self.arrListOfPetsDataFromServer as Any)
        
        self.clView.reloadData()
        
    }
    
}

extension TeacherSelectSubject: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        print("test")
        
    }
    
    // MARK:- DISMISS KEYBOARD -
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == self.clView {
            self.view.endEditing(true)
        }
    }
    
    
}

extension TeacherSelectSubject: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        var sizes: CGSize
                
         /*let result = UIScreen.main.bounds.size
        //NSLog("%f",result.height)
        
        
        if result.height == 480 {
            sizes = CGSize(width: 190, height: 220)
        }
        else if result.height == 568 {
            sizes = CGSize(width: 80, height: 170)
        }
        else if result.height == 667.000000 // 8
        {
            sizes = CGSize(width: 120, height: 190)
        }
        else if result.height == 736.000000 // 8 plus
        {
            sizes = CGSize(width: 170, height: 100)
        }
        else if result.height == 812.000000 // 11 pro
        {
            sizes = CGSize(width: 120, height: 190)
        }
        else if result.height == 896.000000 // 11 , 11 pro max
        {
            sizes = CGSize(width: 120, height: 190)
        }
        else
        {*/
            sizes = CGSize(width: self.view.frame.size.width-20, height: 60)
        // }
        
        
        return sizes
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        // var sizes: CGSize
              
        
         
        
        /*let result = UIScreen.main.bounds.size
        if result.height == 667.000000 { // 8
            return UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        } else if result.height == 736.000000 { // 8 plus
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        } else if result.height == 896.000000 { // 11 plus
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        } else if result.height == 812.000000 { // 11 pro
            return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        } else {*/
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        // }
        
        
            
    }
    
}
