//
//  CompleteProfileWithImageTableCell.swift
//  Ezzycation
//
//  Created by apple on 27/03/21.
//

import UIKit

class CompleteProfileWithImageTableCell: UITableViewCell {

    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.backgroundColor = .white
            viewCellBG.layer.cornerRadius = 6
            viewCellBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var txtAddressWhereTeacherLives:UITextField! {
        didSet {
            txtAddressWhereTeacherLives.layer.cornerRadius = 6
            txtAddressWhereTeacherLives.clipsToBounds = true
            txtAddressWhereTeacherLives.backgroundColor = .white
            txtAddressWhereTeacherLives.setLeftPaddingPoints(20)
            txtAddressWhereTeacherLives.attributedPlaceholder = NSAttributedString(string: "Address where teacher lives",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var viewUploadCardInfo:UIView! {
        didSet {
            viewUploadCardInfo.backgroundColor = .white
            viewUploadCardInfo.layer.cornerRadius = 6
            viewUploadCardInfo.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var viewUploadProfilePicture:UIView! {
        didSet {
            viewUploadProfilePicture.backgroundColor = .white
            viewUploadProfilePicture.layer.cornerRadius = 6
            viewUploadProfilePicture.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnSaveAndContinue:UIButton!  {
        didSet {
            btnSaveAndContinue.backgroundColor = NAVIGATION_COLOR
            btnSaveAndContinue.setTitle("FINISH", for: .normal)
            btnSaveAndContinue.layer.cornerRadius = 6
            btnSaveAndContinue.clipsToBounds = true
            btnSaveAndContinue.setTitleColor(.white, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
