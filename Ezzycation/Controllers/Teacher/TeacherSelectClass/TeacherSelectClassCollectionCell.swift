//
//  TeacherSelectClassCollectionCell.swift
//  Ezzycation
//
//  Created by apple on 01/04/21.
//

import UIKit

class TeacherSelectClassCollectionCell: UICollectionViewCell {
    @IBOutlet weak var lblClass:UILabel!
    @IBOutlet weak var btnClass:UIButton!
}
