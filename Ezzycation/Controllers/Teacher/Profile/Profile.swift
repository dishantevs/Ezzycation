//
//  Profile.swift
//  Ezzycation
//
//  Created by apple on 27/03/21.
//

import UIKit

class Profile: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "Welcome"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var strProfile:String!
    
    @IBOutlet weak var lblMessage:UILabel! {
        didSet {
            lblMessage.text = "Hello, Welcome to EzzyCation!\nContinue as a profile of"
            lblMessage.textAlignment = .center
            lblMessage.textColor = .white
        }
    }
    
    @IBOutlet weak var btnTeacher:UIButton! {
        didSet {
            btnTeacher.backgroundColor = NAVIGATION_COLOR
            btnTeacher.setTitle("Sign In", for: .normal)
            btnTeacher.layer.cornerRadius = 6
            btnTeacher.clipsToBounds = true
            btnTeacher.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var btnStudent:UIButton!  {
        didSet {
            btnStudent.backgroundColor = NAVIGATION_COLOR
            btnStudent.setTitle("Create and Account", for: .normal)
            btnStudent.layer.cornerRadius = 6
            btnStudent.clipsToBounds = true
            btnStudent.setTitleColor(.white, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        if strProfile == "1" {
            lblNavigationTitle.text = "TEACHER PROFILE"
        } else {
            lblNavigationTitle.text = "STUDENT PROFILE"
        }
        
        self.btnTeacher.addTarget(self, action: #selector(teacherClickMethod), for: .touchUpInside)
        self.btnStudent.addTarget(self, action: #selector(studentClickMethod), for: .touchUpInside)
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func teacherClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginId") as? Login
        push!.strTeacherLogin = "1"
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func studentClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginId") as? Login
        push!.strStudentLogin = "2"
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
}
