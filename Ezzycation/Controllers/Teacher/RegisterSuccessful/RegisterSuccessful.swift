//
//  RegisterSuccessful.swift
//  Ezzycation
//
//  Created by apple on 27/03/21.
//

import UIKit

class RegisterSuccessful: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = true
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "REGISTRATION SUCCESSFULL"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    @IBOutlet weak var lblMessage:UILabel! {
        didSet {
            lblMessage.textColor = NAVIGATION_COLOR
        }
    }
    
    @IBOutlet weak var btnPhoneNumber:UIButton! {
        didSet {
            btnPhoneNumber.setTitleColor(NAVIGATION_COLOR, for: .normal)
        }
    }
    
    @IBOutlet weak var btnEmail:UIButton! {
        didSet {
            btnEmail.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var lblMessage2:UILabel! {
        didSet {
            lblMessage2.textColor = UIColor.init(red: 146.0/255.0, green: 145.0/255.0, blue: 173.0/255.0, alpha: 1)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.btnBack.addTarget(self, action: #selector(btnBackClickMethod), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func btnBackClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
}
