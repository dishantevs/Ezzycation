//
//  Welcome.swift
//  Ezzycation
//
//  Created by apple on 27/03/21.
//

import UIKit

class Welcome: UIViewController {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = true
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "Welcome"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    @IBOutlet weak var lblMessage:UILabel! {
        didSet {
            lblMessage.text = "Hello, Welcome to EzzyCation!\nContinue as a profile of"
            lblMessage.textAlignment = .center
            lblMessage.textColor = .white
        }
    }
    
    @IBOutlet weak var btnTeacher:UIButton! {
        didSet {
            btnTeacher.backgroundColor = NAVIGATION_COLOR
            btnTeacher.setTitle("Teacher", for: .normal)
            btnTeacher.layer.cornerRadius = 6
            btnTeacher.clipsToBounds = true
            btnTeacher.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var btnStudent:UIButton!  {
        didSet {
            btnStudent.backgroundColor = NAVIGATION_COLOR
            btnStudent.setTitle("Student", for: .normal)
            btnStudent.layer.cornerRadius = 6
            btnStudent.clipsToBounds = true
            btnStudent.setTitleColor(.white, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.btnTeacher.addTarget(self, action: #selector(teacherClickMethod), for: .touchUpInside)
        self.btnStudent.addTarget(self, action: #selector(studentClickMethod), for: .touchUpInside)
        
        self.rememberMe()
    }
    
    @objc func teacherClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileId") as? Profile
        push!.strProfile = "1"
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func studentClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileId") as? Profile
        push!.strProfile = "2"
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func rememberMe() {
        
        
        
        // let defaults = UserDefaults.standard
        // if let myString = defaults.string(forKey: "rememberMefoReal") {
            // if myString == "1" {
                
                // MARK:- PUSH -
                if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                    print(person as Any)
                        // CompleteProfileId
                    
                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CDashboardId")
                    self.navigationController?.pushViewController(push, animated: true)
                    
                } else {
                    
                }
                
            // } else {
                
            // }
        // }
        
        
    }
    
}
