//
//  SubjectDetailsTableCell.swift
//  Ezzycation
//
//  Created by apple on 30/03/21.
//

import UIKit

class SubjectDetailsTableCell: UITableViewCell {

    @IBOutlet weak var clView:UICollectionView! {
        didSet {
            clView.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var viewBG:UIView!
    @IBOutlet weak var lblSubject:UILabel!
    
    @IBOutlet weak var lblCourseDuration:UILabel! {
        didSet {
            lblCourseDuration.text = "6 Months"
            lblCourseDuration.textColor = NAVIGATION_COLOR
        }
    }
    @IBOutlet weak var lblStudent:UILabel! {
        didSet {
            lblStudent.backgroundColor = NAVIGATION_COLOR
            lblStudent.textAlignment = .center
            lblStudent.text = "0"
            lblStudent.layer.cornerRadius = 20
            lblStudent.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnPrivateClass:UIButton! {
        didSet {
            btnPrivateClass.backgroundColor = NAVIGATION_COLOR
            btnPrivateClass.layer.cornerRadius = 0
            btnPrivateClass.setTitle("Private Class", for: .normal)
            btnPrivateClass.setTitleColor(.white, for: .normal)
            btnPrivateClass.clipsToBounds = true
            
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: btnPrivateClass.frame.height - 1, width: btnPrivateClass.frame.width, height: 2.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            btnPrivateClass.layer.addSublayer(bottomLine)
            
        }
    }
    
    @IBOutlet weak var btnGroupClass:UIButton! {
        didSet {
            btnGroupClass.backgroundColor = NAVIGATION_COLOR
            btnGroupClass.layer.cornerRadius = 0
            btnGroupClass.setTitle("Group Class", for: .normal)
            btnGroupClass.setTitleColor(.white, for: .normal)
            btnGroupClass.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnSelectWorkingTimeFrom:UIButton! {
        didSet {
            btnSelectWorkingTimeFrom.layer.cornerRadius = 6
            btnSelectWorkingTimeFrom.clipsToBounds = true
            btnSelectWorkingTimeFrom.setTitle("From", for: .normal)
            btnSelectWorkingTimeFrom.setTitleColor(.white, for: .normal)
            btnSelectWorkingTimeFrom.backgroundColor = .black
        }
    }
    
    @IBOutlet weak var btnSelectWorkingTimeTo:UIButton! {
        didSet {
            btnSelectWorkingTimeTo.layer.cornerRadius = 6
            btnSelectWorkingTimeTo.clipsToBounds = true
            btnSelectWorkingTimeTo.setTitle("To", for: .normal)
            btnSelectWorkingTimeTo.setTitleColor(.white, for: .normal)
            btnSelectWorkingTimeTo.backgroundColor = .black
        }
    }
    
    @IBOutlet weak var btnSubmit:UIButton! {
        didSet {
            btnSubmit.backgroundColor = NAVIGATION_COLOR
            btnSubmit.setTitle("Update", for: .normal)
            btnSubmit.layer.cornerRadius = 6
            btnSubmit.clipsToBounds = true
            btnSubmit.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var lblTotalStudentsText:UILabel! {
        didSet {
            lblTotalStudentsText.textColor = .white
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
