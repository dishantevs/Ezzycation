//
//  SubjectDetails.swift
//  Ezzycation
//
//  Created by apple on 30/03/21.
//

import UIKit
import Alamofire

class SubjectDetails: UIViewController {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "QUANTUM PHYSICS"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var arrListOfPetsDataFromServer:NSMutableArray = []
    
    let weekdaysArray = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" , "Sunday"]
    
    var strMainSubjectId:String!
    var strCourseDurationId:String!
    
    var strDaySun:String! = "0"
    var strDayMon:String! = "0"
    var strDayTue:String! = "0"
    var strDayWed:String! = "0"
    var strDayThu:String! = "0"
    var strDayFri:String! = "0"
    var strDaySat:String! = "0"
    
    var strStartTime:String! = ""
    var strEndTime:String! = ""
    
    var lblSubject:String!
    var lblSubjectCourseDuration:String!
    
    var strTotalNumberOfStudents:String! = "0"
    
    var strClassType:String!
    
    var strEnrollId:String!
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            self.tbleView.backgroundColor = .white
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.tableFooterView = UIView.init(frame: CGRect.zero)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.btnBack.addTarget(self, action: #selector(btnBackClickMethod), for: .touchUpInside)
        
        self.tbleView.separatorColor = .white
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        print(self.strMainSubjectId as Any)
        
        self.getTeacherEnrollDataWB()
        
        // self.dummyArray()
    }
    
    @objc func btnBackClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    @objc func whenSubjectNotSelectedByTeacher() {
        
        let statusText = "no"
        
        for indexx in 0..<self.weekdaysArray.count {
            
            let item = self.weekdaysArray[indexx]
            let myDictionary: [String:String] = [
                
                "id"        : String(indexx+1),
                "name"      : String(item),
                "status"    : String(statusText)
                
            ]
            
            var res = [[String: String]]()
            res.append(myDictionary)
            
            self.arrListOfPetsDataFromServer.addObjects(from: res)
            
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.reloadData()
            
        }
        
    }
    
    @objc func getTeacherEnrollDataWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
            
        self.view.endEditing(true)
            
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             let x : Int = (person["userId"] as! Int)
             let myString = String(x)
                
            let params = TeacherRollDetails(action: "teacherenrolldetail",
                                            userId: String(myString),
                                            subjectId: String(self.strMainSubjectId),
                                            courseDurationId: String(self.strCourseDurationId))
            
            print(params as Any)
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                              print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                var dict: Dictionary<AnyHashable, Any>
                                dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                self.strEnrollId = "\(dict["id"] as! Int)"
                                
                                self.strDaySun = "\(dict["Sun"] as! Int)"
                                self.strDayMon = "\(dict["Mon"] as! Int)"
                                self.strDayTue = "\(dict["Tue"] as! Int)"
                                self.strDayWed = "\(dict["Wed"] as! Int)"
                                self.strDayThu = "\(dict["Thu"] as! Int)"
                                self.strDayFri = "\(dict["Fri"] as! Int)"
                                self.strDaySat = "\(dict["Sat"] as! Int)"
                                
                                let weekdaysArray2 = [self.strDayMon,
                                                      self.strDayTue,
                                                      self.strDayWed,
                                                      self.strDayThu,
                                                      self.strDayFri,
                                                      self.strDaySat,
                                                      self.strDaySun]
                                
                                let statusTextYes = "yes"
                                let statusTextNo = "no"
                                
                                for indexx in 0..<weekdaysArray2.count {
                                    
                                    let checkNumber = weekdaysArray2[indexx]!
                                    print(checkNumber as Any)
                                    
                                    if String(checkNumber) == "0" {
                                        
                                        let myDictionary: [String:String] = [
                                            
                                            "id"        : String(indexx+1),
                                            "name"      : String(weekdaysArray2[indexx]!),
                                            "status"    : String(statusTextNo)
                                            
                                        ]
                                        
                                        var res = [[String: String]]()
                                        res.append(myDictionary)
                                        self.arrListOfPetsDataFromServer.addObjects(from: res)
                                        
                                    } else {
                                        
                                        let myDictionary: [String:String] = [
                                            
                                            "id"        : String(indexx+1),
                                            "name"      : String(weekdaysArray2[indexx]!),
                                            "status"    : String(statusTextYes)
                                            
                                        ]
                                        
                                        var res = [[String: String]]()
                                        res.append(myDictionary)
                                        self.arrListOfPetsDataFromServer.addObjects(from: res)
                                        
                                    }
                                    
                                    
                                    
                                    
                                }
                                
                                
                                
                                
                                self.strStartTime = "\(dict["StartTime"] as! String)"
                                self.strEndTime = "\(dict["EndTime"] as! String)"
                                
                                
                                var subjectt: Dictionary<AnyHashable, Any>
                                subjectt = dict["subject"] as! Dictionary<AnyHashable, Any>
                                
                                var couseDuration: Dictionary<AnyHashable, Any>
                                couseDuration = dict["courseduration"] as! Dictionary<AnyHashable, Any>
                                
                                self.lblSubject = "\(subjectt["name"] as! String)"+" - "+"\(couseDuration["ClassType"] as! String)"
                                self.lblSubjectCourseDuration = "\(couseDuration["duration"] as! String)"
                                
                                self.strTotalNumberOfStudents = "\(dict["totalStudent"] as! Int)"
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                self.strEnrollId = "0"
                                self.whenSubjectNotSelectedByTeacher()
                                
                                
                                
                                // var strSuccess2 : String!
                                // strSuccess2 = JSON["msg"]as Any as? String
                                
                                // Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            // Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
         }
            
    }
    
    @objc func editTeacherRollWB() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! SubjectDetailsTableCell
        
        if self.strEnrollId == "0" {
            
            self.editEnrollWithoutTeacherEnrollId()
            
        } else {
            
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
                
            self.view.endEditing(true)
                
             if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                 let x : Int = (person["userId"] as! Int)
                 let myString = String(x)
                    
                let params = EditTeacherEnroll(action: "editteacherenroll",
                                               userId: String(myString),
                                               teacherenrollId: self.strEnrollId,
                                               subjectId: String(self.strMainSubjectId),
                                               coursedurationId: String(self.strCourseDurationId),
                                               ClassType: String(strClassType),
                                               Mon: String(self.strDayMon),
                                               Tue: String(self.strDayTue),
                                               Wed: String(self.strDayWed),
                                               Thu: String(self.strDayThu),
                                               Fri: String(self.strDayFri),
                                               Sat: String(self.strDaySat),
                                               Sun: String(self.strDaySun),
                                               StartTime: String((cell.btnSelectWorkingTimeFrom.titleLabel?.text!)!),
                                               EndTime: String((cell.btnSelectWorkingTimeTo.titleLabel?.text!)!))
                
                print(params as Any)
                
                AF.request(APPLICATION_BASE_URL,
                           method: .post,
                           parameters: params,
                           encoder: JSONParameterEncoder.default).responseJSON { response in
                            // debugPrint(response.result)
                            
                            switch response.result {
                            case let .success(value):
                                
                                let JSON = value as! NSDictionary
                                  print(JSON as Any)
                                
                                var strSuccess : String!
                                strSuccess = JSON["status"]as Any as? String
                                
                                // var strSuccess2 : String!
                                // strSuccess2 = JSON["msg"]as Any as? String
                                
                                if strSuccess == String("success") {
                                    print("yes")
                                     // ERProgressHud.sharedInstance.hide()
                                   
                                    self.getTeacherEnrollDataWB()
                                    
                                } else {
                                    
                                    print("no")
                                    ERProgressHud.sharedInstance.hide()
                                    
                                    // var strSuccess2 : String!
                                    // strSuccess2 = JSON["msg"]as Any as? String
                                    
                                    // Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                    
                                }
                                
                            case let .failure(error):
                                print(error)
                                ERProgressHud.sharedInstance.hide()
                                
                                // Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                            }
                    }
             }
            
        }
        
        
            
    }
    
    
    @objc func editEnrollWithoutTeacherEnrollId() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! SubjectDetailsTableCell
        
        
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
            
        self.view.endEditing(true)
            
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             let x : Int = (person["userId"] as! Int)
             let myString = String(x)
                
            let params = EditEnrollWithoutTeacherEnrollId(action: "teacherenroll",
                                           userId: String(myString),
                                           subjectId: String(self.strMainSubjectId),
                                           coursedurationId: String(self.strCourseDurationId),
                                           ClassType: String(strClassType),
                                           Mon: String(self.strDayMon),
                                           Tue: String(self.strDayTue),
                                           Wed: String(self.strDayWed),
                                           Thu: String(self.strDayThu),
                                           Fri: String(self.strDayFri),
                                           Sat: String(self.strDaySat),
                                           Sun: String(self.strDaySun),
                                           StartTime: String((cell.btnSelectWorkingTimeFrom.titleLabel?.text!)!),
                                           EndTime: String((cell.btnSelectWorkingTimeTo.titleLabel?.text!)!))
            
            print(params as Any)
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                             var strSuccess2 : String!
                             strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("Success") {
                                print("yes")
                                 // ERProgressHud.sharedInstance.hide()
                               
                                let alert = UIAlertController(title: String(strSuccess
                                ), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                
                                self.getTeacherEnrollDataWB()
                                
                            } else {
                                
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                 var strSuccess2 : String!
                                 strSuccess2 = JSON["msg"]as Any as? String
                                
                                let alert = UIAlertController(title: String(strSuccess
                                ), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                                    
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                
                                // Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            // Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
         }
            
    }
    
}

//MARK:- TABLE VIEW -
extension SubjectDetails: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if let cell = cell as? SubjectDetailsTableCell {

            cell.clView.dataSource = self
            cell.clView.delegate = self
            cell.clView.tag = indexPath.section
            cell.clView.reloadData()

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SubjectDetailsTableCell = tableView.dequeueReusableCell(withIdentifier: "subjectDetailsTableCell") as! SubjectDetailsTableCell
        
        cell.backgroundColor = .clear
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.btnSelectWorkingTimeFrom.addTarget(self, action: #selector(fromClickMethod), for: .touchUpInside)
        cell.btnSelectWorkingTimeTo.addTarget(self, action: #selector(toClickMethod), for: .touchUpInside)
        
        // cell.btnSubmit.addTarget(self, action: #selector(pushToTeacherEnrolledStudents), for: .touchUpInside)
        
        /*
         self.strStartTime = "\(dict["StartTime"] as! String)"
         self.strEndTime = "\(dict["EndTime"] as! String)"
         */
        
        cell.lblSubject.text = String(self.lblSubject)
        cell.lblCourseDuration.text = String(self.lblSubjectCourseDuration)
        
        cell.btnSelectWorkingTimeFrom.setTitle(String(self.strStartTime), for: .normal)
        cell.btnSelectWorkingTimeTo.setTitle(String(self.strEndTime), for: .normal)
        
        if self.strTotalNumberOfStudents == "0" {
            cell.lblTotalStudentsText.text = "Student"
        } else if self.strTotalNumberOfStudents == "1" {
            cell.lblTotalStudentsText.text = "Student"
        } else {
            cell.lblTotalStudentsText.text = "Students"
        }
        
        cell.lblStudent.text = String(self.strTotalNumberOfStudents)
        
        cell.btnSelectWorkingTimeFrom.isUserInteractionEnabled = true
        cell.btnSelectWorkingTimeTo.isUserInteractionEnabled = true
        
        cell.btnSubmit.addTarget(self, action: #selector(updateValuesClickMethod), for: .touchUpInside)
        
        
        return cell
    }
    
    @objc func updateValuesClickMethod() {
        
        print(self.strDayMon as Any)
        print(self.strDayTue as Any)
        print(self.strDayWed as Any)
        print(self.strDayThu as Any)
        print(self.strDayFri as Any)
        print(self.strDaySat as Any)
        print(self.strDaySun as Any)
        
        if String(self.strDayMon) == "0" &&
            String(self.strDayTue) == "0" &&
            String(self.strDayWed) == "0" &&
            String(self.strDayThu) == "0" &&
            String(self.strDayFri) == "0" &&
            String(self.strDaySat) == "0" &&
            String(self.strDaySun) == "0" {
            
            let alert = UIAlertController(title: String("Alert"), message: String("please select one day."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in

            }))
           
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            self.editTeacherRollWB()
            
        }
        
    }
    
    @objc func pushToTeacherEnrolledStudents() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TeacherEnrolledStudentsId")
        self.navigationController?.pushViewController(push, animated: true)
        
    }
    
    @objc func fromClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! SubjectDetailsTableCell
        
        RPicker.selectDate(title: "Select Time", cancelText: "Cancel", datePickerMode: .time, didSelectDate: { [](selectedDate) in
            
            cell.btnSelectWorkingTimeFrom.setTitle(selectedDate.dateString("hh:mm a"), for: .normal)
            
        })
        
    }
    
    @objc func toClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! SubjectDetailsTableCell
        
        RPicker.selectDate(title: "Select Time", cancelText: "Cancel", datePickerMode: .time, didSelectDate: { [](selectedDate) in
            
            cell.btnSelectWorkingTimeTo.setTitle(selectedDate.dateString("hh:mm a"), for: .normal)
            
        })
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        print("yes called")
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 650
    }
    
}

extension SubjectDetails: UITableViewDelegate {
    
}


// MARK:- COLLECTION VIEW -
extension SubjectDetails: UICollectionViewDelegate {
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
         return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return self.arrListOfPetsDataFromServer.count
       
    }
    
    //Write Delegate Code Here
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subjectDetailsCollectionCell", for: indexPath as IndexPath) as! SubjectDetailsCollectionCell
         
        // MARK:- CELL CLASS -
        cell.layer.cornerRadius = 30
        cell.clipsToBounds = true
        cell.backgroundColor = NAVIGATION_COLOR
        
        // let index = collectionView.tag
        // print(index as Any)
        
        // print(self.addInitialMutable as Any)
        
        let item = self.arrListOfPetsDataFromServer[indexPath.row] as? [String:Any]
        // print(item as Any)
        
        // cell.lblTitle.text = (item!["name"] as! String)
        // cell.btnTitle.setTitle((item!["name"] as! String), for: .normal)
        
        cell.btnTitle.setTitle(String(self.weekdaysArray[indexPath.row]), for: .normal)
        
        if item!["status"] as! String == "yes" {
            
            cell.layer.borderColor = UIColor.white.cgColor
            cell.layer.borderWidth = 0.6
            cell.backgroundColor = NAVIGATION_COLOR
            cell.btnTitle.setTitleColor(.white, for: .normal)
            
        } else {
            
            cell.layer.borderColor = NAVIGATION_COLOR.cgColor
            cell.layer.borderWidth = 0.6
            cell.backgroundColor = .clear
            cell.btnTitle.setTitleColor(.black, for: .normal)
            
        }
        
        
        
        
        // cell.btnTitle.setTitle(String(self.weekdaysArray[indexPath.row]), for: .normal)
        cell.btnTitle.tag = indexPath.row
        cell.btnTitle.isUserInteractionEnabled = true
        cell.btnTitle.addTarget(self, action: #selector(btnTitleClickMethod), for: .touchUpInside)
        
        
        
        return cell
    }
    
    @objc func btnTitleClickMethod(_ sender:UIButton) {
        let btn:UIButton = sender
        
        if btn.tag == 0 {
            
            if self.strDayMon == "0" {
                
                self.strDayMon = "1"
            } else {
                
                self.strDayMon = "0"
                
            }
            
        } else if btn.tag == 1 {
            
            if self.strDayTue == "0" {
                
                self.strDayTue = "1"
            } else {
                
                self.strDayTue = "0"
            }
            
        } else if btn.tag == 2 {
            
            if self.strDayWed == "0" {
                
                self.strDayWed = "1"
            } else {
                
                self.strDayWed = "0"
            
            }
            
        } else if btn.tag == 3 {
            
            if self.strDayThu == "0" {
                
                self.strDayThu = "1"
            } else {
                
                self.strDayThu = "0"
            }
            
        } else if btn.tag == 4 {
            
            if self.strDayFri == "0" {
                
                self.strDayFri = "1"
            } else {
                
                self.strDayFri = "0"
            }
            
        } else if btn.tag == 5 {
            
            if self.strDaySat == "0" {
                
                self.strDaySat = "1"
            } else {
                
                self.strDaySat = "0"
            }
            
        } else {
            
            if self.strDaySun == "0" {
                
                self.strDaySun = "1"
            } else {
                
                self.strDaySun = "0"
            }
            
        }
        
        
        /*let index = collectionView.tag
        print(index as Any)*/
        
        let item = self.arrListOfPetsDataFromServer[btn.tag] as? [String:Any]
        print(item as Any)
        
        if item!["status"] as! String == "no" {
            
            self.arrListOfPetsDataFromServer.removeObject(at: btn.tag)
            
            let myDictionary: [String:String] = [
                
                "id"        : item!["id"] as! String,
                "name"      : item!["name"] as! String,
                "status"    : "yes"
                
            ]
           
            self.arrListOfPetsDataFromServer.insert(myDictionary, at: btn.tag)
            
        } else {
        
            self.arrListOfPetsDataFromServer.removeObject(at: btn.tag)
            
            let myDictionary: [String:String] = [
                
                "id"        : item!["id"] as! String,
                "name"      : item!["name"] as! String,
                "status"    : "no"
                
            ]
           
            self.arrListOfPetsDataFromServer.insert(myDictionary, at: btn.tag)
            
        }
        
        // print(self.arrListOfPetsDataFromServer as Any)
        
        self.tbleView.reloadData()
        
    }
    
    
    
    
    
}

extension SubjectDetails: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("clicked")
        
        
        
    }
    
}

extension SubjectDetails: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var sizes: CGSize
                
        sizes = CGSize(width: 100, height: 60)
        
        return sizes
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 4
        
        /*let result = UIScreen.main.bounds.size
        if result.height == 667.000000 { // 8
            return 2
        } else if result.height == 812.000000 { // 11 pro
            return 4
        } else {
            return 10
        }*/
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
          
    }
    
}
