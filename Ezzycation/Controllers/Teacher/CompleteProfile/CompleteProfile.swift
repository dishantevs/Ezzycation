//
//  CompleteProfile.swift
//  Ezzycation
//
//  Created by apple on 27/03/21.
//

import UIKit

class CompleteProfile: UIViewController {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "COMPLETE PROFILE"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            self.tbleView.backgroundColor = .clear
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.btnBack.addTarget(self, action: #selector(btnBackClickMethod), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func btnBackClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
}

//MARK:- TABLE VIEW -
extension CompleteProfile: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CompleteProfileTableCell = tableView.dequeueReusableCell(withIdentifier: "completeProfileTableCell") as! CompleteProfileTableCell
        
        cell.backgroundColor = .clear
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.btnGender.addTarget(self, action: #selector(genderClickMethod), for: .touchUpInside)
        cell.btnDOB.addTarget(self, action: #selector(dobClickMethod), for: .touchUpInside)
        
        cell.btnSaveAndContinue.addTarget(self, action: #selector(selectSubjectClickMethod), for: .touchUpInside)
        
        return cell
    }
    
    @objc func genderClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! CompleteProfileTableCell
        
        let dummyList = ["Male" , "Female" , "Prefer not to say"]
        RPicker.selectOption(title: "Gender", cancelText: "Cancel", dataArray: dummyList, selectedIndex: 0) {[] (selctedText, atIndex) in
            // TODO: Your implementation for selection
            cell.txtGender.text = selctedText
        }
    }
    
    @objc func dobClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! CompleteProfileTableCell
        
        RPicker.selectDate(title: "Date of Birth", didSelectDate: {[] (selectedDate) in
            // TODO: Your implementation for date
            cell.txtDOB.text = selectedDate.dateString("dd/MM/YYYY")
        })
        
    }
    
    @objc func languageClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! CompleteProfileTableCell
        
        let dummyList = ["English" , "Hindi" ,"German" ,"Spanish" ,"Thai"]
        RPicker.selectOption(title: "Select Language", cancelText: "Cancel", dataArray: dummyList, selectedIndex: 0) {[] (selctedText, atIndex) in
            // TODO: Your implementation for selection
            cell.txtLanguage.text = selctedText
        }
    }
    
    @objc func selectSubjectClickMethod() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectSubjectId")
        self.navigationController?.pushViewController(push, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1000
    }
    
}

extension CompleteProfile: UITableViewDelegate {
    
}
