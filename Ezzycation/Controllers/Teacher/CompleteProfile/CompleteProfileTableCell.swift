//
//  CompleteProfileTableCell.swift
//  Ezzycation
//
//  Created by apple on 27/03/21.
//

import UIKit

class CompleteProfileTableCell: UITableViewCell {

    let paddingDigits:CGFloat = 20
    
    @IBOutlet weak var btnGender:UIButton!
    @IBOutlet weak var txtGender:UITextField! {
        didSet {
            txtGender.layer.cornerRadius = 6
            txtGender.clipsToBounds = true
            txtGender.backgroundColor = .white
            txtGender.setLeftPaddingPoints(paddingDigits)
            txtGender.attributedPlaceholder = NSAttributedString(string: "Gender",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var btnDOB:UIButton!
    @IBOutlet weak var txtDOB:UITextField! {
        didSet {
            txtDOB.layer.cornerRadius = 6
            txtDOB.clipsToBounds = true
            txtDOB.backgroundColor = .white
            txtDOB.setLeftPaddingPoints(paddingDigits)
            txtDOB.attributedPlaceholder = NSAttributedString(string: "DOB",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtWhatTheyWillTeach:UITextField! {
        didSet {
            txtWhatTheyWillTeach.layer.cornerRadius = 6
            txtWhatTheyWillTeach.clipsToBounds = true
            txtWhatTheyWillTeach.backgroundColor = .white
            txtWhatTheyWillTeach.setLeftPaddingPoints(paddingDigits)
            txtWhatTheyWillTeach.attributedPlaceholder = NSAttributedString(string: "What they will teach",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var btnLanguage:UIButton!
    @IBOutlet weak var txtLanguage:UITextField! {
        didSet {
            txtLanguage.layer.cornerRadius = 6
            txtLanguage.clipsToBounds = true
            txtLanguage.backgroundColor = .white
            txtLanguage.setLeftPaddingPoints(paddingDigits)
            txtLanguage.attributedPlaceholder = NSAttributedString(string: "Language",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtExperience:UITextField! {
        didSet {
            txtExperience.layer.cornerRadius = 6
            txtExperience.clipsToBounds = true
            txtExperience.backgroundColor = .white
            txtExperience.setLeftPaddingPoints(paddingDigits)
            txtExperience.attributedPlaceholder = NSAttributedString(string: "Experience",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtDiplomaOrCertificate:UITextField! {
        didSet {
            txtDiplomaOrCertificate.layer.cornerRadius = 6
            txtDiplomaOrCertificate.clipsToBounds = true
            txtDiplomaOrCertificate.backgroundColor = .white
            txtDiplomaOrCertificate.setLeftPaddingPoints(paddingDigits)
            txtDiplomaOrCertificate.attributedPlaceholder = NSAttributedString(string: "Diploma or Certificate",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtAboutYourSelf:UITextView! {
        didSet {
            txtAboutYourSelf.layer.cornerRadius = 6
            txtAboutYourSelf.clipsToBounds = true
            txtAboutYourSelf.backgroundColor = .white
            
        }
    }
    
    @IBOutlet weak var txtSalaryPerAnnum:UITextField! {
        didSet {
            txtSalaryPerAnnum.layer.cornerRadius = 6
            txtSalaryPerAnnum.clipsToBounds = true
            txtSalaryPerAnnum.backgroundColor = .white
            txtSalaryPerAnnum.setLeftPaddingPoints(paddingDigits)
            txtSalaryPerAnnum.attributedPlaceholder = NSAttributedString(string: "Salary per annum ( $ )",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var btnSaveAndContinue:UIButton!  {
        didSet {
            btnSaveAndContinue.backgroundColor = NAVIGATION_COLOR
            btnSaveAndContinue.setTitle("Save & Continue", for: .normal)
            btnSaveAndContinue.layer.cornerRadius = 6
            btnSaveAndContinue.clipsToBounds = true
            btnSaveAndContinue.setTitleColor(.white, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
