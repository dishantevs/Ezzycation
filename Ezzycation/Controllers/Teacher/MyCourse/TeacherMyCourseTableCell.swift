//
//  TeacherMyCourseTableCell.swift
//  Ezzycation
//
//  Created by apple on 27/03/21.
//

import UIKit

class TeacherMyCourseTableCell: UITableViewCell {

    @IBOutlet weak var lblSubjectName:UILabel! {
        didSet {
            lblSubjectName.text = "Quantum Physics -"
        }
    }
    @IBOutlet weak var lblMonths:UILabel! {
        didSet {
            lblMonths.textColor = NAVIGATION_COLOR
        }
    }
    
    @IBOutlet weak var btnClickCell:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
