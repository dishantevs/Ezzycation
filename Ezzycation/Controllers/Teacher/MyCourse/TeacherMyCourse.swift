//
//  TeacherMyCourse.swift
//  Ezzycation
//
//  Created by apple on 27/03/21.
//

import UIKit
import Alamofire

class TeacherMyCourse: UIViewController {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "MY COURSE"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var arrListOfAllMySubjects:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect.zero)
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        if let myLoadedString = UserDefaults.standard.string(forKey: "keyBackOrSlide") {
            if myLoadedString == "backOrMenu" {
                
                // menu
                btnBack.setImage(UIImage(named: "menu"), for: .normal)
                self.sideBarMenuClick()
            } else {
                
                // back
                btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
                btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
            }
            
         } else {
            
            // back
            btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
        
        self.getTeachermyCourses()
    }
    
    @objc func sideBarMenuClick() {
        
        self.view.endEditing(true)
        if revealViewController() != nil {
            
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    @objc func getTeachermyCourses() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
            
        self.view.endEditing(true)
            
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             let x : Int = (person["userId"] as! Int)
             let myString = String(x)
                
            let params = SubjectListWithIdWB(action: "subjectlist",
                                             subjectId: (person["subject"] as! String),
                                             userId: String(myString))
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                              print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllMySubjects.addObjects(from: ar as! [Any])
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                // Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            // Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
         }
            
    }
}

//MARK:- TABLE VIEW -
extension TeacherMyCourse: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListOfAllMySubjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TeacherMyCourseTableCell = tableView.dequeueReusableCell(withIdentifier: "teacherMyCourseTableCell") as! TeacherMyCourseTableCell
        
        cell.backgroundColor = .clear
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.accessoryType = .disclosureIndicator
        
        let item = self.arrListOfAllMySubjects[indexPath.row] as? [String:Any]
        print(item as Any)
        
        cell.lblSubjectName.text = (item!["name"] as! String)
        cell.lblMonths.isHidden = true
        
        cell.btnClickCell.tag = indexPath.row
        cell.btnClickCell.addTarget(self, action: #selector(clickCellMethod), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickCellMethod(_ sender:UIButton) {
        
        let item2 = self.arrListOfAllMySubjects[sender.tag] as? [String:Any]
         // print(item2 as Any)
        
        var ar : NSArray!
        ar = (item2!["Coursedurations"] as! Array<Any>) as NSArray
        
        // print(ar as Any)
        // self.arrListOfAllMySubjects.addObjects(from: ar as! [Any])
        
        let alert = UIAlertController(title: "Please select Duration", message: nil, preferredStyle: .actionSheet)
        
        for indexx in 0..<ar.count {
            
            let item = ar[indexx] as? [String:Any]
            
            
            let x : Int = (item!["price"] as! Int)
            let myString = String(x)
            
            let courseStatement = (item!["duration"] as! String)+" - "+(item!["ClassType"] as! String)+" - $"+String(myString)
            
            alert.addAction(UIAlertAction(title: String(courseStatement), style: .default , handler:{ (UIAlertAction)in
                print("User click Approve button")
                
                // var strMainSubjectId:String!
                // var strCourseDurationId:String!
                
                 // print(item as Any)
                
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubjectDetailsId") as? SubjectDetails
                
                let getSubjectId : Int = (item2!["id"] as! Int)
                let myString = String(getSubjectId)
                
                let getSelectedCourseDurationId : Int = (item!["id"] as! Int)
                let getCID = String(getSelectedCourseDurationId)
                
                push!.strMainSubjectId = String(myString)
                push!.strCourseDurationId = String(getCID)
                
                push!.lblSubject = (item2!["name"] as! String)+" - "+(item!["ClassType"] as! String) // subject name
                push!.lblSubjectCourseDuration = (item!["duration"] as! String) // course duration
                push!.strClassType = (item!["ClassType"] as! String)
                 
                
                
                self.navigationController?.pushViewController(push!, animated: true)
                
            }))
            
        }
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        print("yes called")
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}

extension TeacherMyCourse: UITableViewDelegate {
    
}
