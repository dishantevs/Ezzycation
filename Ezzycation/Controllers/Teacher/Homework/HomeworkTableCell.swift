//
//  HomeworkTableCell.swift
//  Ezzycation
//
//  Created by apple on 30/03/21.
//

import UIKit

class HomeworkTableCell: UITableViewCell {

    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.layer.cornerRadius = 6
            viewBG.clipsToBounds = true
        }
    }
    @IBOutlet weak var imgView:UIImageView! {
        didSet {
            imgView.layer.cornerRadius = 30
            imgView.clipsToBounds = true
            imgView.layer.borderWidth = 0.8
            imgView.layer.borderColor = UIColor.lightGray.cgColor
            imgView.backgroundColor = .lightGray
        }
    }
    
    @IBOutlet weak var lblChapterNumber:UILabel!
    @IBOutlet weak var lblSubject:UILabel! {
        didSet {
            lblSubject.text = "Class - 8th"
        }
    }
    
    @IBOutlet weak var lblMessage:UILabel!
    
    @IBOutlet weak var lblDate:UILabel! {
        didSet {
            lblDate.backgroundColor = .black
            lblDate.textColor = .white
            lblDate.text = "01"
        }
    }
    
    @IBOutlet weak var lblMonth:UILabel! {
        didSet {
            lblMonth.backgroundColor = NAVIGATION_COLOR
            lblMonth.textColor = .white
            lblMonth.text = "June"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
