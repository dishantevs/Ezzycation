//
//  AddNewHomework.swift
//  Ezzycation
//
//  Created by apple on 30/03/21.
//

import UIKit
import Alamofire
import SwiftyJSON

import UniformTypeIdentifiers

class AddNewHomework: UIViewController ,UIDocumentPickerDelegate ,UINavigationControllerDelegate {
    
    
    
    
    
    

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "ADD NEW HOMEWORK"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var arrListOfAllSubjects:NSMutableArray = []
    
    var clickedSubjectIndex:Int!
    var getCourseDurationId:Int!
    
    var imageStr1:String!
    var imgData1:Data!
    
    var strSubjectId:String!
    var strEnrollmentId:String!
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            self.tbleView.backgroundColor = .clear
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.btnBack.addTarget(self, action: #selector(btnBackClickMethod), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        self.callBeforeUI()
        
    }
    
    @objc func btnBackClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    
    @objc func callBeforeUI() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
            
        self.view.endEditing(true)
            
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             let x : Int = (person["userId"] as! Int)
             let myString = String(x)
                
            let params = SubjectListWithIdWB(action: "subjectlist",
                                             subjectId: (person["subject"] as! String),
                                             userId: String(myString))
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                              print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllSubjects.addObjects(from: ar as! [Any])
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                // Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            // Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
         }
            
    }
    
    
    
    @objc func clickCameraGalleryMethod(_ sender:AnyObject) {
            // print("you tap image number: \(sender.view.tag)")
           
           let alert = UIAlertController(title: "Upload", message: nil, preferredStyle: .actionSheet)

           alert.addAction(UIAlertAction(title: "pdf File", style: .default , handler:{ (UIAlertAction)in
               print("User click Approve button")
               // self.openCamera1()
            
            self.openCamera1()
            
           }))

           alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
               print("User click Dismiss button")
           }))

           self.present(alert, animated: true, completion: {
               print("completion block")
           })
           
       }
       
    @objc func openCamera1() {
           
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
        
    }
       
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
         // you get from the urls parameter the urls from the files selected
        
        let cico = url as URL
        print(cico)
        print(url)

        print(url.lastPathComponent)
        
        print(url.pathExtension)
        
        let pdfData = try! Data(contentsOf: url.asURL())
        let data : Data = pdfData
        
        self.imgData1 = data
        print(self.imgData1 as Any)
        
         
        
        
    }
    
    
    @objc func openGallery1() {
       /* let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)*/
           
    }
       
       internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! AddNewHomeworkTableCell
        
        cell.imgHomework.isHidden = false
        let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        cell.imgHomework.image = image_data // show image on profile
        let imageData:Data = image_data!.pngData()!
        self.imageStr1 = imageData.base64EncodedString()
        self.dismiss(animated: true, completion: nil)
        self.imgData1 = image_data!.jpegData(compressionQuality: 0.2)!
               //print(type(of: imgData)) // data
               
        self.imageStr1 = "1"
           
           
            
       }
    
    
    @objc func validationBeforeSubmitHomeworkToWB() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! AddNewHomeworkTableCell
        
        if cell.txtSelectSubject.text == "" {
            
            let alert = UIAlertController(title: "Alert", message: "Subject should not be empty.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "dismiss", style: .destructive, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtDuration.text == "" {
            
            let alert = UIAlertController(title: "Alert", message: "Duration should not be empty.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "dismiss", style: .destructive, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtTaskName.text == "" {
            
            let alert = UIAlertController(title: "Alert", message: "Task Name should not be empty.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "dismiss", style: .destructive, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtView.text == "" {
            
            let alert = UIAlertController(title: "Alert", message: "Comment should not be empty.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "dismiss", style: .destructive, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            self.addHomeworkOnOurServer()
            
        }
    }
    
    @objc func addHomeworkOnOurServer() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! AddNewHomeworkTableCell
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
          //Set Your URL
        let api_url = APPLICATION_BASE_URL
        guard let url = URL(string: api_url) else {
            return
        }

        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // let str:String = person["role"] as! String
           
            let x : Int = person["userId"] as! Int
            let myString = String(x)
               
            var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
            urlRequest.httpMethod = "POST"
            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")

           //Set Your Parameter
            let parameterDict = NSMutableDictionary()
            parameterDict.setValue("addhomework", forKey: "action")
            parameterDict.setValue(String(myString), forKey: "userId")
            // parameterDict.setValue("\(self.getCourseDurationId!)", forKey: "courseDurationId")
            parameterDict.setValue(String(cell.txtTaskName.text!), forKey: "TaskName")
            parameterDict.setValue(String(cell.txtView.text!), forKey: "comment")
              
            parameterDict.setValue(String(self.strEnrollmentId), forKey: "teacherEnrollId")
            parameterDict.setValue(String(self.strSubjectId), forKey: "subjectId")
            
            
            
          // Now Execute
            AF.upload(multipartFormData: { multipartFormData in
                    
                    for (key, value) in parameterDict {
                        if let temp = value as? String {
                            multipartFormData.append(temp.data(using: .utf8)!, withName: key as! String)
                        }
                        if let temp = value as? Int {
                            multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key as! String)
                        }
                        if let temp = value as? NSArray {
                            temp.forEach({ element in
                                let keyObj = key as! String + "[]"
                                if let string = element as? String {
                                    multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                                } else
                                    if let num = element as? Int {
                                        let value = "\(num)"
                                        multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                                }
                            })
                        }
                    }
                    
                if let data = self.imgData1 {
                        multipartFormData.append(data, withName: "PDF", fileName: "\(Date.init().timeIntervalSince1970).pdf", mimeType: "application/pdf")
                    }
                },
                          to: APPLICATION_BASE_URL, method: .post , headers: ["Authorization": "auth_token"])
                    .responseJSON(completionHandler: { (response) in
                        
                        print(response)
                        
                        if let err = response.error{
                            print(err)
                            // onError?(err)
                            return
                        }
                        print("Succesfully uploaded")
                        
                        let json = response.data
                        
                        if (json != nil)
                        {
                            let jsonObject = JSON(json!)
                            print(jsonObject)
                            
                            ERProgressHud.sharedInstance.hide()
                            
                            let alert = UIAlertController(title: "Success", message: "Homework added successfully.", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { action in
                                self.navigationController?.popViewController(animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                    })
              }
    }
}

//MARK:- TABLE VIEW -
extension AddNewHomework: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AddNewHomeworkTableCell = tableView.dequeueReusableCell(withIdentifier: "addNewHomeworkTableCell") as! AddNewHomeworkTableCell
        
        cell.backgroundColor = .clear
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.btnSelectSubject.addTarget(self, action: #selector(subjectListClickMethod), for: .touchUpInside)
        cell.btnSelectCourseEnroll.addTarget(self, action: #selector(enrollClickMethod), for: .touchUpInside)
        
        cell.btnUploadHomeworkFile.addTarget(self, action: #selector(clickCameraGalleryMethod), for: .touchUpInside)
        
        cell.btnSubmit.addTarget(self, action: #selector(validationBeforeSubmitHomeworkToWB), for: .touchUpInside)
        
        return cell
    }
    
    @objc func subjectListClickMethod() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! AddNewHomeworkTableCell
        
        let alert = UIAlertController(title: "Please select Subject", message: nil, preferredStyle: .actionSheet)
        
        for indexx in 0..<self.arrListOfAllSubjects.count {
            
            let item = self.arrListOfAllSubjects[indexx] as? [String:Any]
             print(item as Any)
            
            
            // self.arrListOfAllSubjects.addObjects(from: ar as! [Any])
            
            alert.addAction(UIAlertAction(title: (item!["name"] as! String), style: .default , handler:{ (UIAlertAction)in
                print("User click Approve button")
                
                cell.txtSelectSubject.text = (item!["name"] as! String)
                
                self.clickedSubjectIndex = indexx
                
                self.strSubjectId = "\(item!["id"] as! Int)"
                // print(self.strSubjectId as Any)
                
                // var strEnrollmentId:String!
                
                // print(self.clickedSubjectIndex as Any)
                
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
         
    }
    
    @objc func enrollClickMethod() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! AddNewHomeworkTableCell
        
        if cell.txtSelectSubject.text == "" {
            
            let alert = UIAlertController(title: "Success", message: "Please select subject first", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            let alert = UIAlertController(title: "Please select Duration", message: nil, preferredStyle: .actionSheet)
            
            
            let item = self.arrListOfAllSubjects[self.clickedSubjectIndex] as? [String:Any]
            // print(item as Any)
            
            var ar : NSArray!
            ar = (item!["TeacherEnroll"] as! Array<Any>) as NSArray
            
            if ar.count == 0 {
                
                let alert = UIAlertController(title: String("Alert"), message: String("No enrollment found for this Subject."), preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
                
            } else {
               
                for indexx in 0..<ar.count {
                    
                    let item2 = ar[indexx] as? [String:Any]
                    
                    let statementIs = (item2!["duration"] as! String)+" - "+(item2!["ClassType"] as! String)
                    alert.addAction(UIAlertAction(title: String(statementIs), style: .default , handler:{ (UIAlertAction)in
                        print("User click Approve button")
                        
                        cell.txtDuration.text = String(statementIs)
                        
                        self.strEnrollmentId = "\(item2!["teacherEnrollId"] as! Int)"
                        
                        // self.getCourseDurationId = (item2!["id"] as! Int)
                    }))
                    
                }
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                    print("User click Dismiss button")
                }))

                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
                
            }
            
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 800
    }
    
}

extension AddNewHomework: UITableViewDelegate {
    
}


/*extension AddNewHomework: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {

        let cico = url as URL
        print(cico)
        print(url)

        print(url.lastPathComponent)

        print(url.pathExtension)

    }
}*/
