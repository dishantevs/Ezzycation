//
//  AddNewHomeworkTableCell.swift
//  Ezzycation
//
//  Created by apple on 30/03/21.
//

import UIKit

class AddNewHomeworkTableCell: UITableViewCell {

    @IBOutlet weak var txtSelectSubject:UITextField! {
        didSet {
            txtSelectSubject.layer.cornerRadius = 6
            txtSelectSubject.clipsToBounds = true
            txtSelectSubject.backgroundColor = .white
            txtSelectSubject.setLeftPaddingPoints(20)
            txtSelectSubject.attributedPlaceholder = NSAttributedString(string: "select subject...",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtDuration:UITextField! {
        didSet {
            txtDuration.layer.cornerRadius = 6
            txtDuration.clipsToBounds = true
            txtDuration.backgroundColor = .white
            txtDuration.setLeftPaddingPoints(20)
            txtDuration.attributedPlaceholder = NSAttributedString(string: "course duration...",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtTaskName:UITextField! {
        didSet {
            txtTaskName.layer.cornerRadius = 6
            txtTaskName.clipsToBounds = true
            txtTaskName.backgroundColor = .white
            txtTaskName.setLeftPaddingPoints(20)
            txtTaskName.attributedPlaceholder = NSAttributedString(string: "task name...",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtView:UITextView! {
        didSet {
            txtView.text = ""
            txtView.layer.cornerRadius = 8
        }
    }
    
    @IBOutlet weak var btnSelectSubject:UIButton!
    @IBOutlet weak var btnSelectCourseEnroll:UIButton!
    
    @IBOutlet weak var btnSubmit:UIButton!  {
        didSet {
            btnSubmit.backgroundColor = NAVIGATION_COLOR
            btnSubmit.setTitle("Submit", for: .normal)
            btnSubmit.layer.cornerRadius = 6
            btnSubmit.clipsToBounds = true
            btnSubmit.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var viewBGG:UIView! {
        didSet {
            viewBGG.backgroundColor = .white
            viewBGG.layer.cornerRadius = 8
            viewBGG.clipsToBounds = true
            viewBGG.layer.borderWidth = 1
            viewBGG.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    @IBOutlet weak var btnUploadHomeworkFile:UIButton!
    
    @IBOutlet weak var imgHomework:UIImageView! {
        didSet {
            imgHomework.isHidden = true
            imgHomework.backgroundColor = .white
            imgHomework.layer.cornerRadius = 8
            imgHomework.clipsToBounds = true
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
