//
//  Homework.swift
//  Ezzycation
//
//  Created by apple on 30/03/21.
//

import UIKit
import Alamofire

class Homework: UIViewController {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "HOMEWORK"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var arrListOfHomeworks:NSMutableArray = []
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect.zero)
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
        }
    }
    
    @IBOutlet weak var btnAddNewHomework:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        // self.btnBack.addTarget(self, action: #selector(btnBackClickMethod), for: .touchUpInside)
        
        self.btnAddNewHomework.addTarget(self, action: #selector(addNewHomeworkClickMethod), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        if let myLoadedString = UserDefaults.standard.string(forKey: "keyBackOrSlide") {
            if myLoadedString == "backOrMenu" {
                
                // menu
                btnBack.setImage(UIImage(named: "menu"), for: .normal)
                self.sideBarMenuClick()
            } else {
                
                // back
                btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
                btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
            }
            
         } else {
            
            // back
            btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.homeworkLisWB()
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnBackClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc func sideBarMenuClick() {
        
        self.view.endEditing(true)
        if revealViewController() != nil {
            
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
    }
    
    @objc func addNewHomeworkClickMethod() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewHomeworkId")
        self.navigationController?.pushViewController(push, animated: true)
        
    }
    
    
    // MARK:- HOME WORK LIST -
    @objc func homeworkLisWB() {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
        self.arrListOfHomeworks.removeAllObjects()
        
        self.view.endEditing(true)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         // let str:String = person["role"] as! String
        
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
            let params = HomeworkListWB(action: "homeworklist",
                                              userId: String(myString),
                                              userType: (person["role"] as! String)
            )
        
            print(params as Any)
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                        switch response.result {
                        case let .success(value):
                        
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                        
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        /*
                         
                         */
                        
                            if strSuccess == String("success") {
                                print("yes")
                                ERProgressHud.sharedInstance.hide()
                           
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfHomeworks.addObjects(from: ar as! [Any])
                            
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                            
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                            
                                /*var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String*/
                            
                            // Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                            }
                        
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                        
                        // Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                       }
        }
    }
    
}

//MARK:- TABLE VIEW -
extension Homework: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        /*var numOfSection: NSInteger = 0

        if self.arrListOfHomeworks.count > 0 {

            self.tbleView.backgroundView = nil
            numOfSection = 1
                    
        }
        else {

            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: self.tbleView.bounds.size.width, height: tbleView.bounds.size.height))
            noDataLabel.text          = "No Homework"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            noDataLabel.numberOfLines = 0
            self.tbleView.backgroundView  = noDataLabel
            self.tbleView.separatorStyle  = .none

        }

        return numOfSection*/
        
        
        return self.arrListOfHomeworks.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:HomeworkTableCell = tableView.dequeueReusableCell(withIdentifier: "homeworkTableCell") as! HomeworkTableCell
        
        cell.backgroundColor = .clear
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.accessoryType = .disclosureIndicator
        
        let item = self.arrListOfHomeworks[indexPath.row] as? [String:Any]
        
        let fullNameArr = (item!["created"] as! String).components(separatedBy: "-")

        // let expMonth    = fullNameArr[0]
        let dateDown = fullNameArr[1]
        let dateUp = fullNameArr[2]
        
        // print(expMonth as Any)
        // print(dateDown as Any)
        // print(dateUp as Any)
        
        cell.lblChapterNumber.text = (item!["TaskName"] as! String)
        cell.lblChapterNumber.text = (item!["TaskName"] as! String)
        
        let removeSpaceFromDate = dateUp.components(separatedBy: " ")
        let finalDateUpIs    = removeSpaceFromDate[0]
        cell.lblDate.text = String(finalDateUpIs)
        
        
        if String(dateDown) == "1" || String(dateDown) == "01"  {
            
            cell.lblMonth.text = "JAN"
            
        } else if String(dateDown) == "2"  || String(dateDown) == "02" {
            
            cell.lblMonth.text = "FEB"
            
        } else if String(dateDown) == "3" || String(dateDown) == "03" {
            
            cell.lblMonth.text = "MAR"
            
        } else if String(dateDown) == "4" || String(dateDown) == "04" {
            
            cell.lblMonth.text = "APR"
            
        } else if String(dateDown) == "5" || String(dateDown) == "05" {
            
            cell.lblMonth.text = "MAY"
            
        } else if String(dateDown) == "6" || String(dateDown) == "06" {
            
            cell.lblMonth.text = "JUN"
            
        } else if String(dateDown) == "7" || String(dateDown) == "07" {
            
            cell.lblMonth.text = "JUL"
            
        } else if String(dateDown) == "8" || String(dateDown) == "08" {
            
            cell.lblMonth.text = "AUG"
            
        } else if String(dateDown) == "9" || String(dateDown) == "09" {
            
            cell.lblMonth.text = "SEP"
            
        } else if String(dateDown) == "10" {
            
            cell.lblMonth.text = "OCT"
            
        } else if String(dateDown) == "11" {
            
            cell.lblMonth.text = "NOV"
            
        } else if String(dateDown) == "12" {
            
            cell.lblMonth.text = "DEC"
            
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        print("yes called")
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

extension Homework: UITableViewDelegate {
    
}
