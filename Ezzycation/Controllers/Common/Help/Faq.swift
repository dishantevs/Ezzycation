//
//  Faq.swift
//  CuckooBirds
//
//  Created by Apple on 12/12/20.
//

import UIKit
import Alamofire

class Faq: UIViewController, UITextFieldDelegate, UISearchBarDelegate {
    
    // ***************************************************************** // nav
            
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .black
            }
        }
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "HELP"
                lblNavigationTitle.textColor = .white
            }
        }
            
    // ***************************************************************** // nav
    
     var arrListOfAllMyFAQs:NSMutableArray! = []
    
    var filterdata:[String:Any]!
    @IBOutlet var searchview: UISearchBar!
    
    let cellReuseIdentifier = "faqTableCell"
    
    var searchArray : NSArray! = []
    var searchArrRes = [[String:Any]]()
    var originalArr = [[String:Any]]();
    var searching:Bool = false
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var strIndexNumber:Int!
    var strChangeColorForTitle:Int!
    
    var dictevent = Dictionary<String , Any>()
    
    
    @IBOutlet weak var viewBGforSearch:UIView! {
        didSet {
            viewBGforSearch.layer.cornerRadius = 0
            viewBGforSearch.clipsToBounds = true
            viewBGforSearch.backgroundColor = NAVIGATION_COLOR
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .black
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var txtSearch:UITextField! {
        didSet {
            txtSearch.backgroundColor = .white
            txtSearch.layer.cornerRadius = 6
            txtSearch.clipsToBounds = true
            txtSearch.setLeftPaddingPoints(40)
            txtSearch.placeholder = "search..."
            
            txtSearch.backgroundColor = .white
            txtSearch.textColor = .black
            txtSearch.attributedPlaceholder = NSAttributedString(string: "search...",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // searchview.delegate = self
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.strIndexNumber = 1200
          
        if let myLoadedString = UserDefaults.standard.string(forKey: "keyBackOrSlide") {
            if myLoadedString == "backOrMenu" {
                
                // menu
                btnBack.setImage(UIImage(named: "menu"), for: .normal)
                self.sideBarMenuClick()
                
            } else {
                
                // back
                btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
                btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
            }
            
         } else {
            
            // back
            btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
        
        self.tbleView.separatorColor = .white
        
        self.txtSearch.delegate = self
        
        self.tbleView.tableFooterView = UIView() // Removes empty cell separators
        self.tbleView.estimatedRowHeight = 60
        self.tbleView.rowHeight = UITableView.automaticDimension
        
        self.view.backgroundColor = .white
        
        self.faqWBwb()
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        //input text
        let searchText  = textField.text! + string
        searchArrRes = self.originalArr.filter({(($0["answer"] as! String).localizedCaseInsensitiveContains(searchText))})
        
        if(searchArrRes.count == 0) {
            searching = false
        } else {
            searching = true
        }
        
        self.tbleView.reloadData();
        
        return true
    }
    
    @objc func sideBarMenuClick() {
        
        let defaults = UserDefaults.standard
        defaults.setValue("", forKey: "keyBackOrSlide")
        defaults.setValue(nil, forKey: "keyBackOrSlide")
        
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                  
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    @objc func faqWBwb() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
            
        self.view.endEditing(true)
            
        // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // let x : Int = (person["userId"] as! Int)
            // let myString = String(x)
                
            let params = FAQWB(action: "faq")
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                              print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                
                                /*var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])*/
                                
                                
                                
                                self.originalArr = JSON["data"] as! [[String : Any]]
                                
                                print(self.originalArr as Any)
                                
                                
                                
                                
                                
                                
                                
                                // self.filterdata = self.arrListOfAllMyOrders
                                  // print(self.arrListOfAllMyOrders)
                                
                                /*
                                for faqCheck in 0..<self.arrListOfAllMyOrders.count {
                                     print(faqCheck as Any)
                                    
                                    let item = self.arrListOfAllMyOrders[faqCheck] as? [String:Any]
                                    
                                    self.datasource = [ExpandingTableViewCellContent(
                                                        
                                    title: item!["question"] as! String,
                                    subtitle: item!["answer"] as! String
                                    )]
                                    
                                    self.arrListOfAllMyFAQs.addObjects(from: self.datasource)
                                }*/
                                
                                // print(self.datasource.count as Any)
                                
                                /*self.datasource = [ExpandingTableViewCellContent(title: "Vestibulum id ligula porta felis euismod semper.",
                                                                            subtitle: "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas sed diam eget risus varius blandit sit amet non magna."),
                                              ExpandingTableViewCellContent(title: "Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis .",
                                                                            subtitle: "Etiam porta sem malesuada magna mollis euismod. Nullam quis risus urna mollis ornare vel eu leo."),
                                              ExpandingTableViewCellContent(title: "Aenean lacinia bibendum nulla sed consectetur.",
                                                                            subtitle: "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget metus.")]*/
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                // Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            // Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
            
            
    }
    
    
     
    
    
}

extension Faq: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return self.arrListOfAllMyOrders.count
        
        if (searching == true) {
            
            return searchArrRes.count
            
        } else {
            
            return originalArr.count
            
            /*if originalArr.count == 0 {
                return 0
            }
            else  if originalArr.count <= 10
            {
                return originalArr.count
            }
            else
            {
                return 10
            }*/
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell:FaqTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! FaqTableCell
         
        // print(self.strChangeColorForTitle as Any)
        
        // print(indexPath.row as Any)
        
        if self.strChangeColorForTitle != indexPath.row {
            cell.lblTitle.textColor = .white
        } else {
            cell.lblTitle.textColor = NAVIGATION_COLOR
        }
        
        if(searching == true) {
            // dictevent = searchArrRes[indexPath.row] as? Dictionary<String, Any> ?? Dictionary<String, Any> ()
            // let item = self.searchArrRes[indexPath.row] as? [String:Any]
            
            print(dictevent["answer"] as! String)
            dictevent = searchArrRes[indexPath.row] // ?? Dictionary<String, Any> ()
            
            cell.lblTitle.text = (dictevent["question"] as! String)
            
            let htmlString = "<span style=\"font-family: Avenir Next;color:white; font-size: 16.0\">\((dictevent["answer"] as! String) )</span>"
            let data = htmlString.data(using: String.Encoding.unicode)! // mind "!"
            let attrStr = try? NSAttributedString( // do catch
                data: data,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            // suppose we have an UILabel, but any element with NSAttributedString will do
            cell.lblSubTitle.attributedText = attrStr
            cell.lblSubTitle.textColor = .white // NAVIGATION_TITLE_COLOR
            
            // cell.lblName.text =  dictevent["title"] as? String ?? ""
        }
        else {
            
            /*let item = self.originalArr[indexPath.row] //as? [String:Any]
            print(item as Any)*/
            
            dictevent = originalArr[indexPath.row]
            // print(dictevent["answer"] as! String)
            
            cell.lblTitle.text = (dictevent["question"] as! String)
            
            
            let htmlString = "<span style=\"font-family: Avenir Next;color:white; font-size: 16.0\">\((dictevent["answer"] as! String) )</span>"
            let data = htmlString.data(using: String.Encoding.unicode)! // mind "!"
            let attrStr = try? NSAttributedString( // do catch
                data: data,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            // suppose we have an UILabel, but any element with NSAttributedString will do
            cell.lblSubTitle.attributedText = attrStr
            cell.lblSubTitle.textColor = .white // NAVIGATION_TITLE_COLOR
        }
        
        
        
        
        
        cell.tag = indexPath.row
        cell.backgroundColor = .black
        
        return cell
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        
        if(searching == true) {
            
            dictevent = searchArrRes[indexPath.row]
                 
            if self.strIndexNumber == indexPath.row {
                print("yes same")
                self.strIndexNumber = 1200
            } else {
                print("i am different")
                self.strIndexNumber = indexPath.row
                
                self.strChangeColorForTitle = indexPath.row
            }
            
        }
        else {
           
            dictevent = originalArr[indexPath.row]
           
            
            if self.strIndexNumber == indexPath.row {
                print("yes same")
                self.strIndexNumber = 1200
            } else {
                print("i am different")
                self.strIndexNumber = indexPath.row
                
                self.strChangeColorForTitle = indexPath.row
            }
            
        }
        
        
        
       
        self.tbleView.reloadData()
        
        // let content = datasource[indexPath.row]
        // content.expanded = !content.expanded
        // tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == self.strIndexNumber {
            return UITableView.automaticDimension
        } else {
            return 60
        }
        
    }
    
}

extension Faq: UITableViewDelegate {
    
}
