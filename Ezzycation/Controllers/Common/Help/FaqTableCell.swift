//
//  FaqTableCell.swift
//  CuckooBirds
//
//  Created by Apple on 12/12/20.
//

import UIKit

class FaqTableCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel! {
        didSet {
            lblTitle.textColor = .white
        }
    }
    @IBOutlet weak var lblSubTitle:UILabel! {
        didSet {
            lblSubTitle.textColor = .white
        }
    }
    
    @IBOutlet weak var btnImgArrow:UIButton! {
        didSet {
            btnImgArrow.tintColor = .white // NAVIGATION_COLOR
            btnImgArrow.isUserInteractionEnabled = false
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
