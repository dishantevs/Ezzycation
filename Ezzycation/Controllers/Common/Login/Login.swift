//
//  Login.swift
//  Ezzycation
//
//  Created by apple on 27/03/21.
//

import UIKit
import Alamofire

class Login: UIViewController {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = true
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "Login"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var strTeacherLogin:String!
    var strStudentLogin:String!
    
    var myDeviceTokenIs:String!
    
    @IBOutlet weak var lblMessage:UILabel! {
        didSet {
            lblMessage.text = "Hello user, welcome back!\n Login in your account"
            lblMessage.textAlignment = .center
            lblMessage.textColor = .white
        }
    }
    
    @IBOutlet weak var txtUsername:UITextField! {
        didSet {
            txtUsername.layer.cornerRadius = 6
            txtUsername.clipsToBounds = true
            txtUsername.backgroundColor = .white
            txtUsername.setLeftPaddingPoints(40)
            txtUsername.textColor = .black
            txtUsername.attributedPlaceholder = NSAttributedString(string: "Username",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtPassword:UITextField! {
        didSet {
            txtPassword.layer.cornerRadius = 6
            txtPassword.clipsToBounds = true
            txtPassword.backgroundColor = .white
            txtPassword.setLeftPaddingPoints(40)
            txtPassword.textColor = .black
            txtPassword.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var btnSignIn:UIButton!  {
        didSet {
            btnSignIn.backgroundColor = NAVIGATION_COLOR
            btnSignIn.setTitle("Sign In", for: .normal)
            btnSignIn.layer.cornerRadius = 6
            btnSignIn.clipsToBounds = true
            btnSignIn.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var btnForgotPassword:UIButton! {
        didSet {
            btnForgotPassword.setTitleColor(.white, for: .normal)
            btnForgotPassword.setTitle("Forgot Password", for: .normal)
        }
    }
    
    @IBOutlet weak var btnDontHaveAnAccount:UIButton! {
        didSet {
            btnDontHaveAnAccount.setTitleColor(NAVIGATION_COLOR, for: .normal)
            btnDontHaveAnAccount.setTitle("Don't have an account ? - Sign Up", for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.btnBack.addTarget(self, action: #selector(btnBackClickMethod), for: .touchUpInside)
        self.btnSignIn.addTarget(self, action: #selector(pushToDashboardScreen), for: .touchUpInside)
        self.btnDontHaveAnAccount.addTarget(self, action: #selector(dontHaveAnAccountClickMethod), for: .touchUpInside)
        
        // keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @objc func btnBackClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dontHaveAnAccountClickMethod() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegistrationId")
        self.navigationController?.pushViewController(push, animated: true)
        
    }
    
    @objc func pushToDashboardScreen() {
        
        if String(self.txtUsername.text!) == "" {
            
        } else if String(self.txtPassword.text!) == "" {
            
        } else {
        
            self.loginWB()
            
        }
        
        
        /*let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StudentDashboardId")
        self.navigationController?.pushViewController(push, animated: true)*/
        
    }
    
    @objc func loginWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        // Create UserDefaults
        let defaults = UserDefaults.standard
        if let myString = defaults.string(forKey: "deviceFirebaseToken") {
            self.myDeviceTokenIs = myString

        }
        else {
            self.myDeviceTokenIs = "111111111111111111111"
        }
        
        self.view.endEditing(true)
        
        /*
         [action] => login
             [email] => student163@gmail.com
             [password] => 12345678
         */
        
        let params = LoginWB(action: "login",
                             email: String(self.txtUsername.text!),
                             password: String(self.txtPassword.text!),
                             device: "iOS",
                             deviceToken: String(self.myDeviceTokenIs))
        
        print(params as Any)
        
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var dict: Dictionary<AnyHashable, Any>
                            dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                             let defaults = UserDefaults.standard
                             defaults.setValue(dict, forKey: "keyLoginFullData")
                            
                            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CDashboardId")
                            self.navigationController?.pushViewController(push, animated: true)
                            
                        } else {
                            
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        
                    }
        }
        
    }
    
}
