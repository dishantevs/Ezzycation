//
//  Registration.swift
//  Ezzycation
//
//  Created by apple on 27/03/21.
//

import UIKit
import Alamofire

class Registration: UIViewController {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .white
                btnBack.isHidden = false
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "Registraiton"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var myDeviceTokenIs:String!
    
    @IBOutlet weak var txtName:UITextField! {
        didSet {
            txtName.layer.cornerRadius = 6
            txtName.clipsToBounds = true
            txtName.backgroundColor = .white
            txtName.setLeftPaddingPoints(40)
            txtName.textColor = .black
            
            txtName.attributedPlaceholder = NSAttributedString(string: "Name",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtEmailAddress:UITextField! {
        didSet {
            txtEmailAddress.layer.cornerRadius = 6
            txtEmailAddress.clipsToBounds = true
            txtEmailAddress.backgroundColor = .white
            txtEmailAddress.setLeftPaddingPoints(40)
            txtEmailAddress.textColor = .black
            
            txtEmailAddress.attributedPlaceholder = NSAttributedString(string: "Email address",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtPassword:UITextField! {
        didSet {
            txtPassword.layer.cornerRadius = 6
            txtPassword.clipsToBounds = true
            txtPassword.backgroundColor = .white
            txtPassword.setLeftPaddingPoints(40)
            txtPassword.textColor = .black
            
            txtPassword.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            
            
        }
    }
    
    @IBOutlet weak var txtPhone:UITextField! {
        didSet {
            txtPhone.layer.cornerRadius = 6
            txtPhone.clipsToBounds = true
            txtPhone.backgroundColor = .white
            txtPhone.setLeftPaddingPoints(40)
            
            txtPhone.attributedPlaceholder = NSAttributedString(string: "Phone",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            
            txtPhone.textColor = .black
        }
    }
    
    
    @IBOutlet weak var btnSignIn:UIButton!  {
        didSet {
            btnSignIn.backgroundColor = NAVIGATION_COLOR
            btnSignIn.setTitle("Sign Up", for: .normal)
            btnSignIn.layer.cornerRadius = 6
            btnSignIn.clipsToBounds = true
            btnSignIn.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var btnDontHaveAnAccount:UIButton! {
        didSet {
            btnDontHaveAnAccount.setTitleColor(NAVIGATION_COLOR, for: .normal)
            btnDontHaveAnAccount.setTitle("Already have an account ? - Sign In", for: .normal)
        }
    }
    
    @IBOutlet weak var lblMessage:UILabel! {
        didSet {
            lblMessage.text = "Hello user, welcome back!\n Login in your account"
            lblMessage.textAlignment = .center
            lblMessage.textColor = .white
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = VIEW_BACKGROUND
        
        self.btnSignIn.addTarget(self, action: #selector(signInClickMethod), for: .touchUpInside)
        
        self.btnDontHaveAnAccount.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // MARK:- VIEW UP WHEN CLICK ON TEXT FIELD -
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- KEYBOARD WILL SHOW -
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    // MARK:- KEYBOARD WILL HIDE -
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func signInClickMethod() {
        
        
        
        /*let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StudentCompleteProfileId")
        self.navigationController?.pushViewController(push, animated: true)*/
        
        self.registrationTeacherWB()
    }
    
    
    @objc func registrationTeacherWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        
        // Create UserDefaults
        let defaults = UserDefaults.standard
        if let myString = defaults.string(forKey: "deviceFirebaseToken") {
            self.myDeviceTokenIs = myString

        }
        else {
            self.myDeviceTokenIs = "111111111111111111111"
        }
        
        self.view.endEditing(true)
        
        /*
         [action] => login
             [email] => student163@gmail.com
             [password] => 12345678
         */
        
        
        
        
        let params = FullTeacherRegistration(action: "registration",
                                             email: String(txtEmailAddress.text!),
                                             fullName: String(txtName.text!),
                                             contactNumber: String(txtPhone.text!),
                                             password: String(txtPassword.text!),
                                             role: "Teacher",
                                             device: "iOS",
                                             deviceToken: String(myDeviceTokenIs))
        
        print(params as Any)
        
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var dict: Dictionary<AnyHashable, Any>
                            dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                             let defaults = UserDefaults.standard
                             defaults.setValue(dict, forKey: "keyLoginFullData")
                            
                            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TeacherSelectClassId")
                            self.navigationController?.pushViewController(push, animated: true)
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        
                    }
                   }
        
    }
    
}
