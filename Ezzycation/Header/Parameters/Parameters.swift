//
//  Parameters.swift
//  KamCash
//
//  Created by Apple on 16/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit


/* SCHOOL */

struct FullTeacherRegistration: Encodable {
    let action: String
    let email: String
    let fullName: String
    let contactNumber: String
    let password: String
    let role: String
    let device: String
    let deviceToken: String
}

// MARK:- CLASS LIST -
struct ClassWB: Encodable {
    let action: String
}

// MARK:- SUBJECT LIST -
struct SubjectListWB: Encodable {
    let action: String
}

// MARK:- SUBJECT LIST WITH ID -
struct SubjectListWithIdWB: Encodable {
    let action: String
    let subjectId: String
    let userId: String
}

// MARK:- SUBJECT LIST WITH ID -
struct TeacherRollDetails: Encodable {
    let action: String
    let userId: String
    let subjectId: String
    let courseDurationId: String
}

// MARK:- SUBJECT LIST WITH ID -
struct EditTeacherEnroll: Encodable {
    let action: String
    let userId: String
    let teacherenrollId: String
    let subjectId: String
    let coursedurationId: String
    let ClassType: String
    let Mon: String
    let Tue: String
    let Wed: String
    let Thu: String
    let Fri: String
    let Sat: String
    let Sun: String
    let StartTime: String
    let EndTime: String
}

// MARK:- SUBJECT LIST WITH ID -
struct EditEnrollWithoutTeacherEnrollId: Encodable {
    let action: String
    let userId: String
    let subjectId: String
    let coursedurationId: String
    let ClassType: String
    let Mon: String
    let Tue: String
    let Wed: String
    let Thu: String
    let Fri: String
    let Sat: String
    let Sun: String
    let StartTime: String
    let EndTime: String
}

struct FAQWB: Encodable {
    let action: String
}

// MARK:- CLASS LIST -
struct TeacherEnrollmentListWB: Encodable {
    let action: String
    let userId: String
}


// MARK:- LOGIN -
struct LoginWB: Encodable {
    let action: String
    let email: String
    let password: String
    let device: String
    let deviceToken: String
}

// edit user without image
struct EditUserWithoutImage: Encodable {
    let action: String
    let userId: String
    let fullName: String
    let contactNumber: String
    let address: String
}

// homework list
struct HomeworkListWB: Encodable {
    let action: String
    let userId: String
    let userType: String
}




/* STUDENT */
struct StudentCoursePurchaseList: Encodable {
    let action: String
    let userId: String
    let userType: String
}

// all subject lists
struct StudentSubjectListWB: Encodable {
    let action: String
    let userId: String
    let subjectId: String
}


// teachers list
struct StudentAllTeacherListWB: Encodable {
    let action: String
    let courseDurationId: String
}


// logout
struct LogoutFromApp: Encodable {
    let action: String
    let userId: String
}

class Parameters: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
