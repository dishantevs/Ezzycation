//
//  MenuControllerVC.swift
//  SidebarMenu
//
//  Created by Apple  on 16/10/19.
//  Copyright © 2019 AppCoda. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class MenuControllerVC: UIViewController {

    let cellReuseIdentifier = "menuControllerVCTableCell"
    
    var bgImage: UIImageView?
    
    var roleIs:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
    
    @IBOutlet weak var viewUnderNavigation:UIView! {
        didSet {
            viewUnderNavigation.backgroundColor = .black
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "MENU"
            lblNavigationTitle.textColor = .white
        }
    }
    
    @IBOutlet weak var imgSidebarMenuImage:UIImageView! {
        didSet {
            imgSidebarMenuImage.backgroundColor = .clear
            imgSidebarMenuImage.layer.cornerRadius = 35
            imgSidebarMenuImage.clipsToBounds = true
        }
    }
    
    // teacher
    var arrSidebarMenuTeacherProfileDeliveryServiceTitle = ["Dashboard", "Edit profile", "Homeworks", "Course/Exams" , "Course/Exam Timing" , "Chat" , "Help" , "Change Password" , "Logout"]
    var arrSidebarMenuTeacherProfileDeliveryServiceImage = ["home","edit2","note","note","note", "chat" , "faq" , "lock" , "logout"]
    
    
    
    // student
    var arrSidebarMenuStudentProfileDeliveryServiceTitle = ["Dashboard",
                                                            "Edit profile",
                                                            "Course/Exams",
                                                            "My Books",
                                                            "Homeworks",
                                                            "Online Library",
                                                            "Chat",
                                                            "Notes",
                                                            "Help",
                                                            "Change Password",
                                                            "Logout"]
    var arrSidebarMenuStudentProfileDeliveryServiceImage = ["home","edit2","course1","book"/*,"note","note"*/,"reminder","reminder","chat3","video","faq","lock","logout"]
    
    
    
    @IBOutlet weak var lblUserName:UILabel! {
        didSet {
            lblUserName.text = "Dishant Rajput"
            lblUserName.textColor = .white
        }
    }
    
    @IBOutlet weak var lblPhoneNumber:UILabel! {
        didSet {
            lblPhoneNumber.textColor = .white
        }
    }
    
    @IBOutlet var menuButton:UIButton!
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
            tbleView.tableFooterView = UIView.init()
            tbleView.backgroundColor = NAVIGATION_COLOR
            // tbleView.separatorInset = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
            tbleView.separatorColor = .white
        }
    }
    
    @IBOutlet weak var lblMainTitle:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideBarMenuClick()
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        self.tbleView.separatorColor = .white

        self.view.backgroundColor = .white
        
        self.sideBarMenuClick()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
             if person["role"] as! String == "Teacher" {
                
                self.lblUserName.text = (person["fullName"] as! String)
                self.lblPhoneNumber.text = (person["contactNumber"] as! String)
                
             } else {
                
                self.lblUserName.text = (person["fullName"] as! String)
                self.lblPhoneNumber.text = (person["contactNumber"] as! String)
                
                // imgSidebarMenuImage.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "logo"))
            }
             
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @objc func sideBarMenuClick() {
        
        if revealViewController() != nil {
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
}

extension MenuControllerVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        /*if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        
            if person["role"] as! String == "Driver" {
                return arrSidebarMenuUserProfileDeliveryServiceTitle.count
            } else {
                return arrSidebarMenuTitle.count
            }
            
        } else {
            return 0
        }*/
        
        
        return arrSidebarMenuTeacherProfileDeliveryServiceTitle.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MenuControllerVCTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MenuControllerVCTableCell
        
        cell.backgroundColor = .clear
      
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        
            if person["role"] as! String == "Teacher" {
            
                cell.lblName.text = arrSidebarMenuTeacherProfileDeliveryServiceTitle[indexPath.row]
                cell.lblName.textColor = .black
                cell.imgProfile.backgroundColor = .orange
                cell.imgProfile.image = UIImage(named: arrSidebarMenuTeacherProfileDeliveryServiceImage[indexPath.row])
                cell.imgProfile.backgroundColor = .clear
            } else {
                
                cell.lblName.text = arrSidebarMenuStudentProfileDeliveryServiceTitle[indexPath.row]
                cell.lblName.textColor = .black
                cell.imgProfile.backgroundColor = .orange
                cell.imgProfile.image = UIImage(named: arrSidebarMenuStudentProfileDeliveryServiceImage[indexPath.row])
                cell.imgProfile.backgroundColor = .clear
            }
            
        } // else {
        
            
        
        // }
        
        
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        
            if person["role"] as! String == "Teacher" {
        
                if arrSidebarMenuTeacherProfileDeliveryServiceTitle[indexPath.row] == "Dashboard" {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                    self.view.window?.rootViewController = sw
                    let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "CDashboardId")
                    let navigationController = UINavigationController(rootViewController: destinationController!)
                    sw.setFront(navigationController, animated: true)
                    
                } else if arrSidebarMenuTeacherProfileDeliveryServiceTitle[indexPath.row] == "Edit profile" {
                    
                    let myString = "backOrMenu"
                    UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                    self.view.window?.rootViewController = sw
                    let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "CEditProfileId")
                    let navigationController = UINavigationController(rootViewController: destinationController!)
                    sw.setFront(navigationController, animated: true)
                    
                } else if arrSidebarMenuTeacherProfileDeliveryServiceTitle[indexPath.row] == "Homeworks" {
                    
                    let myString = "backOrMenu"
                    UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                    self.view.window?.rootViewController = sw
                    let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "HomeworkId")
                    let navigationController = UINavigationController(rootViewController: destinationController!)
                    sw.setFront(navigationController, animated: true)
                    
                } else if arrSidebarMenuTeacherProfileDeliveryServiceTitle[indexPath.row] == "Help" {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                    self.view.window?.rootViewController = sw
                    let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "FaqId")
                    let navigationController = UINavigationController(rootViewController: destinationController!)
                    sw.setFront(navigationController, animated: true)
                    
                } else if arrSidebarMenuTeacherProfileDeliveryServiceTitle[indexPath.row] == "Course/Exams" {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                    self.view.window?.rootViewController = sw
                    let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "TeacherMyCourseId")
                    let navigationController = UINavigationController(rootViewController: destinationController!)
                    sw.setFront(navigationController, animated: true)
                    
                } else if arrSidebarMenuTeacherProfileDeliveryServiceTitle[indexPath.row] == "Course/Exam Timing" {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                    self.view.window?.rootViewController = sw
                    let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "TeacherEnrollementListId")
                    let navigationController = UINavigationController(rootViewController: destinationController!)
                    sw.setFront(navigationController, animated: true)
                    
                } else if arrSidebarMenuTeacherProfileDeliveryServiceTitle[indexPath.row] == "Logout" {
                    
                    let alert = UIAlertController(title: String("Logout"), message: String("Are you sure you want to logout ?"), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Yes, Logout", style: .default, handler: { action in
                         self.logoutWB()
                    }))
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                     }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
                // student
            } else if arrSidebarMenuStudentProfileDeliveryServiceTitle[indexPath.row] == "Dashboard" {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "StudentDashboardId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
                
            }  else if arrSidebarMenuStudentProfileDeliveryServiceTitle[indexPath.row] == "Edit profile" {
                
                let myString = "backOrMenu"
                UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "CEditProfileId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
                
            }  else if arrSidebarMenuStudentProfileDeliveryServiceTitle[indexPath.row] == "Course/Exams" {
                
                let myString = "backOrMenu"
                UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "StudentMyCourseId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
                
            }
        
            
        }
        
    }
    
    
    @objc func logoutWB() {
         ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         // let str:String = person["role"] as! String
        
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
        let params = LogoutFromApp(action: "logout",
                                   userId: String(myString))
        
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                          // print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            let defaults = UserDefaults.standard
                            defaults.setValue("", forKey: "keyLoginFullData")
                            defaults.setValue(nil, forKey: "keyLoginFullData")

                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                            self.view.window?.rootViewController = sw
                            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeId")
                            let navigationController = UINavigationController(rootViewController: destinationController!)
                            sw.setFront(navigationController, animated: true)
                            
                        } else {
                            
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            // Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        // Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
    }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension MenuControllerVC: UITableViewDelegate {
    
}
